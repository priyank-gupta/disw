import React from "react";
import Headline from "../../../shared/Journeys/Headline/Headline";
import RelatedLinks from "../../../shared/Journeys/RelatedLinks/RelatedLinks";
import Carousel from "../../../shared/Journeys/Carousel/FourItemCarousel";
import companyImg from "./img/electra-meccanica-company.png";
import videoImg from "./img/electromeccanica.png";
import video from "./video/electra-meccanica .mp4";
import { journeyCarouselTitle, journeyCarouselItems } from "../../../Constants";
import SocialShare from "../../../shared/SocialShare/SocialShare";

import SEOtags from "../../../SEOTags";
import twitterImage from "./img/electrical-electra-meccanica-siemens-1200X1200.jpg";
import ogImage from "./img/electrical-electra-meccanica-siemens-1200X630.jpg";

const AmericaMakesContent = {

  headTitle: {
    title: "Electrical",
    lines: [
      "From napkin sketch to a fully functional three-wheel electric automobile. Electra Meccanica and Siemens do the unimaginable.",
    ],
  },

  headline: {
    video: {
      id: undefined,
      url: video,
      image: videoImg,
    },

    section: {
      title: { label: "Company", value: "Electra Meccanica" },
      industry: { label: "Industry", value: { text: "Automotive", url: "https://www.plm.automation.siemens.com/global/en/industries/automotive-transportation/" } },
      product: { label: "Product", value: "Electric Vehicle" },
      size: { label: "Size", value: "Small" },
      location: { label: "Location", value: "Canada" },
      // year: "2017",
      software: { label: "Siemens Software", value: { text: "Simcenter", href: "https://www.plm.automation.siemens.com/global/en/products/simcenter/" } },
      software2: { label: "Siemens Software", value: { text: "NX", href: "https://www.plm.automation.siemens.com/global/en/products/nx/" } },
      todaysTechnology: { label: "Today's Technology", value: { text: "Integrated CAD/CAE", url: "https://www.plm.automation.siemens.com/global/en/products/simcenter/" } },
      tommorowsVision: { label: "Tomorrow's Vision", value: "Desktop Design and Manufacturing" },
      trialBtn: { text: "Try NX CAM free", url: "https://trials.ias.plm.automation.siemens.com/nx-cam/" },
    },

    hashTagAndLinks: [
      {
        link: "https://twitter.com/search?q=%23todaymeetstomorrow",
        linkText: "#todaymeetstomorrow"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/simcenter/",
        linkText: "simcenter"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/nx/",
        linkText: "nx"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/industries/automotive-transportation/",
        linkText: "automotive"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/glossary/digitalization/25216",
        linkText: "digitalization"
      }
    ],

    quote: {
      text: "There truly is a revolution in productivity because of the Siemens software that we’re using.",
      who: "Jerry Kroll",
      what: "CEO, Electra Meccanica Vehicles Corp.",
      image: companyImg,
    },
  },

  podCast: {
    title: "Related Links",
    links: [
      {
        link:
          "https://new.siemens.com/global/en/company/stories/industry/boom-this-car-s-ready-to-go.html",
        linkText:
          "Boom! This car’s ready to go!",
        podCastHeading: "Newsroom ",
        icon: "book-open"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/customers/electra-meccanica/53556/",
        linkText: "Electra Meccanica success story",
        podCastHeading: "Success Story ",
        icon: "thumbs-up"
      },
      {
        link: "https://community.plm.automation.siemens.com/t5/Simcenter-Blog/Creating-an-Electric-Vehicle-in-Two-Years-It-s-Possible/ba-p/466476",
        linkText: "Creating an Electric Vehicle in Two Years? It's Possible!",
        podCastHeading: "Community ",
        icon: "user-friends"
      },
    ],
  },

  carousel: {
    title: journeyCarouselTitle,
    items: journeyCarouselItems,
  },

};

class AmericaMakes extends React.Component {
  render() {
    const { headTitle, headline, podCast, carousel } = AmericaMakesContent;
    return (
      <>
      <SEOtags
            seoTitle= "Electrical"
            seoDescription= "To create a fully functional three-wheel electrical automobile, a small automotive company in Canada used Siemens integrated CAD/CAE software."
            canonicalUrl="/journeys/electrical/"
            twitterImage= {twitterImage}
            ogImage={ogImage}
      />
      <div>
        <div className="container">
          <div className="row mt-4">
            <div className="col">
              <SocialShare elm={<h1 className="pt-4 pb-3">{headTitle.title}</h1>} />
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 pb-4">
              {headTitle.lines.map((subHeadLine, i) => {
                return <h3 key={i}>{subHeadLine}</h3>;
              })}
            </div>
          </div>
        </div>
        <Headline
          videoUrl={headline.video.url}
          videoPoster={headline.video.image}
          hashTagAndLinks={headline.hashTagAndLinks}
          title={headline.section.title}
          industry={headline.section.industry}
          product={headline.section.product}
          size={headline.section.size}
          location={headline.section.location}
          year={headline.section.year}
          software={headline.section.software}
          software2={headline.section.software2}
          todaysTechnology={headline.section.todaysTechnology}
          tommorowsVision={headline.section.tommorowsVision}
          trialBtnText={headline.section.trialBtn.text}
          trialBtnUrl={headline.section.trialBtn.url}
          quote={headline.quote.text}
          quotePersonName={headline.quote.who}
          quotePersonPosition={headline.quote.what}
          quoteCompanyImg={headline.quote.image}
        />
        <RelatedLinks
          title={podCast.title}
          podCastLinks={podCast.links}
        />
        <Carousel
          headline={carousel.title}
          carouselItems={carousel.items}
        />
      </div>
      </>
    );
  }
}

export default AmericaMakes;
