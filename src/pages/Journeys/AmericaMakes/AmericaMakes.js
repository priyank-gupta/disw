import React from "react";
import Headline from "../../../shared/Journeys/Headline/Headline";
import RelatedLinks from "../../../shared/Journeys/RelatedLinks/RelatedLinks";
import Carousel from "../../../shared/Journeys/Carousel/FourItemCarousel";
import companyImg from "./img/america-makes-company.png";
import videoImg from "./img/america-makes-video.png";
import video from "./video/america-makes-video.mp4";
import { journeyCarouselTitle, journeyCarouselItems, relatedLinkIcon } from "../../../Constants";
import SocialShare from "../../../shared/SocialShare/SocialShare";

import SEOtags from "../../../SEOTags";
import twitterImage from "./img/industrial-additive-cloud-america-makes-siemens-1200X1200.jpg";
import ogImage from "./img/industrial-additive-cloud-america-makes-siemens-1200X630.jpg";

const AmericaMakesContent = {

  headTitle: {
    title: "Industry Additive with Cloud",
    lines: [
      "America Makes partners with Siemens to drive adoption of additive manufacturing (via the Industrial Internet of Things).",
    ],
  },

  headline: {
    video: {
      id: undefined,
      url: video,
      image: videoImg,
    },

    section: {
      title: { label: "Company", value: "America Makes" },
      industry: { label: "Industry", value: { text: "Manufacturing", url: "https://www.plm.automation.siemens.com/global/en/products/manufacturing-planning/" } },
      product: { label: "Product", value: "Digital Storefront" },
      size: { label: "Size", value: "Small" },
      location: { label: "Location", value: "United States" },
      software: { label: "Siemens Software", value: { text: "Teamcenter", url: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/" } },
      todaysTechnology: { label: "Today's Technology", value: { text: "Additive Manufacturing", url: "https://www.plm.automation.siemens.com/global/en/products/manufacturing-planning/additive-manufacturing.html" } },
      tommorowsVision: { label: "Tomorrow's Vision", value: "Industrial Additive" },
      trialBtn: { text: "Teamcenter", url: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/", },
    },

    hashTagAndLinks: [
      {
        link: "https://twitter.com/search?q=%23todaymeetstomorrow",
        linkText: "#todaymeetstomorrow"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/glossary/digital-manufacturing/13157",
        linkText: "digital manufacturing"
      },
      {
        link: "https://www.americamakes.us/our_work/#Work_DSF",
        linkText: "digital storefront"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/glossary/industrial-internet-of-things/57242",
        linkText: "IIoT"
      },
      {
        link: "/portfolio/cloud", linkText: "cloud"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/",
        linkText: "teamcenter"
      }
    ],

    quote: {
      text: "Siemens has and continues to be a great partner to us from a sales, education, support and services perspective.",
      who: "Joe Veranese",
      what: "Business Systems Director, The National Center for Defense Manufacturing and Machining",
      image: companyImg,
    },

  },

  podCast: {
    title: "Related Links",
    links: [
      {
        link:
          "https://aws.amazon.com/partners/apn-journal/all/america-makes-siemens-plm-software/",
        linkText:
          "America Makes Transforms the Additive Manufacturing Industry and Saves Millions on AWS",
        podCastHeading: "Newsroom",
        icon: relatedLinkIcon["Newsroom"]
      },
      {
        link:
          "https://community.plm.automation.siemens.com/siemensplm/attachments/siemensplm/Teamcenter-Blog/468/1/Siemens-PLM-America-Makes-case-study-infographic-70235-A10.pdf",
        linkText:
          "America Makes: A Digital Storefront for additive manufacturing using PLM on the Cloud",
        podCastHeading: "Whitepaper",
        icon: relatedLinkIcon["Whitepaper"]
      },
      {
        link: "",
        linkText: "",
        podCastHeading: "Blogpost",
        icon: relatedLinkIcon["Blogpost"]
      },
      {
        link:
          "https://community.plm.automation.siemens.com/t5/Teamcenter-Blog/Building-Additive-Manufacturing-Industry-Teamcenter-amp-Active/ba-p/510099",
        linkText:
          "Building Additive Manufacturing Industry - Teamcenter & Active Workspace",
        podCastHeading: "Community",
        icon: relatedLinkIcon["Community"]
      },
      {
        link: "",
        linkText: "",
        podCastHeading: "Podcast",
        icon: relatedLinkIcon["Podcast"]
      }
    ],
  },

  carousel: {
    title: journeyCarouselTitle,
    items: journeyCarouselItems,
  },

};

class AmericaMakes extends React.Component {
  render() {
    const { headTitle, headline, podCast, carousel } = AmericaMakesContent;
    return (
      <>
       <SEOtags
            seoTitle= "Industrial Additive with Cloud"
            seoDescription= "Partnering with Siemens, a small public-private partnership adopted industrial additive with cloud to create a digital storefront, saving millions of dollars."
            canonicalUrl="/journeys/industrial-additive-cloud/"
            twitterImage= {twitterImage}
            ogImage={ogImage}
      />
      <div>
        <div className="container">
          <div className="row mt-4">
            <div className="col">
              <SocialShare elm={<h1 className="pt-4 pb-3">{headTitle.title}</h1>} />
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 pb-4">
              {headTitle.lines.map((subHeadLine, i) => {
                return <h3 key={i}>{subHeadLine}</h3>;
              })}
            </div>
          </div>
        </div>
        <Headline
          videoUrl={headline.video.url}
          videoPoster={headline.video.image}
          hashTagAndLinks={headline.hashTagAndLinks}
          title={headline.section.title}
          industry={headline.section.industry}
          product={headline.section.product}
          size={headline.section.size}
          location={headline.section.location}
          year={headline.section.year}
          software={headline.section.software}
          todaysTechnology={headline.section.todaysTechnology}
          tommorowsVision={headline.section.tommorowsVision}
          trialBtnText={headline.section.trialBtn.text}
          trialBtnUrl={headline.section.trialBtn.url}
          quote={headline.quote.text}
          quotePersonName={headline.quote.who}
          quotePersonPosition={headline.quote.what}
          quoteCompanyImg={headline.quote.image}
        />
        <RelatedLinks title={podCast.title} podCastLinks={podCast.links} />
        <Carousel
          headline={carousel.title}
          carouselItems={carousel.items}
        />
      </div>
      </>
    );
  }
}

export default AmericaMakes;
