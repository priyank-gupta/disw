import React from "react";
import Headline from "../../../shared/Journeys/Headline/Headline";
import RelatedLinks from "../../../shared/Journeys/RelatedLinks/RelatedLinks";
import Carousel from "../../../shared/Journeys/Carousel/FourItemCarousel";
import companyImg from "./img/electrolux-company.png";
import videoImg from "./img/electrolux.png";
import video from "./video/Electrolux.mp4";
import { journeyCarouselTitle, journeyCarouselItems } from "../../../Constants";
import SocialShare from "../../../shared/SocialShare/SocialShare";

import SEOtags from "../../../SEOTags";
import twitterImage from "./img/industry-4.0-electrolux-siemens-1200X1200.jpg";
import ogImage from "./img/industry-4.0-electrolux-siemens-1200X630.jpg";

const ElectroluxContent = {

  headTitle: {
    title: "Industry 4.0",
    lines: [
      "Embracing Industry 4.0 and digital manufacturing: How Siemens helped Electrolux improve time-to-market and reduce production costs."
    ],
  },

  headline: {
    video: {
      id: undefined,
      url: video,
      image: videoImg,
    },

    section: {
      title: { label: "Company", value: "Electrolux" },
      industry: { label: "Industry", value: { text: "Manufacturing", url: "https://www.plm.automation.siemens.com/global/en/our-story/glossary/industry-4-0/29278" } },
      product: { label: "Product", value: "Commercial & Consumer Appliances " },
      size: { label: "Size", value: "Large" },
      location: { label: "Location", value: "Sweden" },
      //year: "2017",
      software: { label: "Siemens Software", value: { text: "Tecnomatix", url: "https://www.plm.automation.siemens.com/global/en/products/tecnomatix/ " } },
      software2: { label: "Siemens Software", value: { text: "Teamcenter", url: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/ " } },
      todaysTechnology: { label: "Today's Technology", value: { text: "Factory Planning ", url: "https://www.plm.automation.siemens.com/global/en/products/manufacturing-planning/ " } },
      tommorowsVision: { label: "Tomorrow's Vision", value: "Consumer-driven Manufacturing " },
      trialBtn: { text: "Free Plant Simulation trial ", url: "https://www.plm.automation.siemens.com/store/en-us/trial/plant-simulation.html " },
    },

    hashTagAndLinks: [
      {
        link: "https://twitter.com/search?q=%23todaymeetstomorrow",
        linkText: "#todaymeetstomorrow"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/glossary/digital-manufacturing/13157",
        linkText: "digital manufacturing"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/industries/electronics-semiconductors/home-appliances-white-goods/home-appliance-design.html",
        linkText: "appliances"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/manufacturing-planning/",
        linkText: "factory planning"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/glossary/industrial-internet-of-things/57242",
        linkText: "IIoT"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/tecnomatix/",
        linkText: "tecnomatix"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/",
        linkText: "teamcenter"
      },
      
    ],

    quote: {
      text: "With the good visualization possibilities of Tecnomatix, I can show the management an early stage of planning that makes the processes plausible.",
      who: "Bernd Ebert",
      what: "Director of Global Manufacturing Engineering − Food Preparation, Electrolux",
      image: companyImg,
    },
  },

  podCast: {
    title: "Related Links",
    links: [
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/customers/electrolux/57807/",
        linkText: "Electrolux success story",
        podCastHeading: "Success Story ",
        icon: "thumbs-up"
      },
      {
        link: "https://community.plm.automation.siemens.com/t5/Teamcenter-Blog/Electrolux-implements-digital-manufacturing-worldwide/ba-p/575914",
        linkText: "Electrolux implements digital manufacturing worldwide",
        podCastHeading: "Community",
        icon: "user-friends"
      },
      {
        link: "https://www.youtube.com/watch?v=aN_sk9eVwnQ",
        linkText: "Electrolux Shape living for the better! A customer interview",
        podCastHeading: "Podcast ",
        icon: "podcast"
      }
    ],
  },

  carousel: {
    title: journeyCarouselTitle,
    items: journeyCarouselItems,
  },
};

class Electrolux extends React.Component {
  render() {
    const { headTitle, headline, podCast, carousel } = ElectroluxContent;
    return (
      <>
       <SEOtags
            seoTitle= "Industry 4.0"
            seoDescription= "With Industry 4.0 and digital manufacturing for factory planning, Siemens helped a large manufacturing company reduce time-to-market and production costs."
            canonicalUrl="/journeys/industry/"
            twitterImage= {twitterImage}
            ogImage={ogImage}
      />
      <div>
        <div className="container">
          <div className="row mt-4">
            <div className="col">
              <SocialShare elm={<h1 className="pt-4 pb-3">{headTitle.title}</h1>} />
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 pb-4">
              {headTitle.lines.map((subHeadLine, i) => {
                return <h3 key={i}>{subHeadLine}</h3>;
              })}
            </div>
          </div>
        </div>
        <Headline
          videoUrl={headline.video.url}
          videoPoster={headline.video.image}
          hashTagAndLinks={headline.hashTagAndLinks}
          title={headline.section.title}
          industry={headline.section.industry}
          product={headline.section.product}
          size={headline.section.size}
          location={headline.section.location}
          year={headline.section.year}
          software={headline.section.software}
          software2={headline.section.software2}
          todaysTechnology={headline.section.todaysTechnology}
          tommorowsVision={headline.section.tommorowsVision}
          trialBtnText={headline.section.trialBtn.text}
          trialBtnUrl={headline.section.trialBtn.url}
          quote={headline.quote.text}
          quotePersonName={headline.quote.who}
          quotePersonPosition={headline.quote.what}
          quoteCompanyImg={headline.quote.image}
        />
        <RelatedLinks
          title={podCast.title}
          podCastLinks={podCast.links}
        />
        <Carousel
          headline={carousel.title}
          carouselItems={carousel.items}
        />
      </div>
      </>
    );
  }
}

export default Electrolux;
