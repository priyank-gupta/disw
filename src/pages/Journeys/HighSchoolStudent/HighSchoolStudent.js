import React from "react";
import Headline from "../../../shared/Journeys/Headline/Headline";
import RelatedLinks from "../../../shared/Journeys/RelatedLinks/RelatedLinks";
import Carousel from "../../../shared/Journeys/Carousel/FourItemCarousel";
import SocialShare from "../../../shared/SocialShare/SocialShare";
// import companyImg from "./img/high-school-student-company.png";
import videoImg from "./img/high-school-student-video.jpg";
import video from "./video/high-school-student-video.mp4";
import { journeyCarouselTitle, journeyCarouselItems, relatedLinkIcon } from "../../../Constants";

import SEOtags from "../../../SEOTags";
import twitterImage from "./img/generative-engineering-ashley-kimbel-siemens-1200X1200.jpg";
import ogImage from "./img/generative-engineering-ashley-kimbel-siemens-1200X630.jpg";

const GenerativeDesignContent = {

  headTitle: {
    title: "Generative Engineering",
    lines: [
      "17-year-old student designs, tests and builds a highly streamlined prosthetic foot for injured U.S. Marine."
    ],
  },

  headline: {
    video: {
      id: undefined,
      url: video,
      image: videoImg,
    },

    section: {
      title: { label: "High School Student", value: "Ashley Kimbel" },
      industry: { label: "Industry", value: { text: "Medical", href: "https://www.plm.automation.siemens.com/global/en/industries/medical-devices-pharmaceuticals/" } },
      product: { label: "Product", value: "Prosthetics" },
      size: "",
      location: { label: "Location", value: "United States" },
      year: undefined,
      software: { label: "Siemens Software", value: { text: "Solid Edge", href: "https://solidedge.siemens.com/en/" } },
      todaysTechnology: { label: "Today's Technology", value: { text: "Generative Design", href: "https://solidedge.siemens.com/en/solutions/products/3d-design/next-generation-design/generative-design/" } },
      tommorowsVision: { label: "Tomorrow's Vision", value: "Next Generation Engineering" },
      trialBtn: { text: "Try Solid Edge free", url: "https://www.plm.automation.siemens.com/store/en-us/trial/solid-edge.html" },
    },

    hashTagAndLinks: [
      {
        link: "https://twitter.com/search?q=%23todaymeetstomorrow",
        linkText: "#todaymeetstomorrow"
      },
      {
        link: "https://twitter.com/search?q=%23humanlypossible",
        linkText: "#humanlypossible"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/industries/medical-devices-pharmaceuticals/",
        linkText: "medical"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/mechanical-design/generative-design.html",
        linkText: "generative design"
      },
      {
        link: "https://new.siemens.com/us/en/company/jobs/life-at-siemens/veterans.html",
        linkText: "veterans"
      },
      {
        link: "https://solidedge.siemens.com/en/",
        linkText: "solid edge"
      },
    ],

    quote: {
      text: "In the future, I want to do the things that are impossible right now. If I can design this prosthetic now, I can't wait to see what I will design in the future.",
      who: "Ashley Kimbel",
      what: "Prosthetic Designer, Grissom High School",
      image: undefined,
    },

  },

  podCast: {
    title: "Related Links",
    links: [
      {
        link:
          "https://news.usa.siemens.biz/blog/young-people-can-do-more-technology-change-world-ever-heres-proof",
        linkText:
          "Young people can do more with technology to change the world than ever before. Here’s proof.",
        podCastHeading: "Newsroom",
        icon: relatedLinkIcon["Newsroom"]
      },
      {
        link: "",
        linkText: "",
        podCastHeading: "Whitepaper",
        icon: relatedLinkIcon["Whitepaper"]
      },
      {
        link:
          "https://community.plm.automation.siemens.com/t5/Academic-News/From-the-TODAY-show-A-high-school-student-and-Solid-Edge-help-an/ba-p/574660",
        linkText:
          "From the TODAY show: A high school student (and Solid Edge) help an injured veteran get moving again",
        podCastHeading: "Community",
        icon: relatedLinkIcon["Community"]
      },
      {
        link: "",
        linkText: "",
        podCastHeading: "Podcast",
        icon: relatedLinkIcon["Podcast"]
      },
    ]
  },

  carousel: {
    title: journeyCarouselTitle,
    items: journeyCarouselItems,
  },

};

class HighSchoolStudent extends React.Component {
  render() {
    const { headTitle, headline, podCast, carousel } = GenerativeDesignContent;
    return (
      <>
       <SEOtags
            seoTitle= "Generative Engineering"
            seoDescription= "A high school student used generative engineering design to build a prosthetic foot for a U.S. Marine amputee using Solid Edge software from Siemens."
            canonicalUrl="/journeys/generative-engineering/"
            twitterImage= {twitterImage}
            ogImage={ogImage}
      />
      <div>
        <div className="container">
          <div className="row mt-4">
            <div className="col">
              <SocialShare elm={<h1 className="pt-4 pb-3">{headTitle.title}</h1>} />
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 pb-4">
              {headTitle.lines.map((subHeadLine, i) => {
                return <h3 key={i}>{subHeadLine}</h3>;
              })}
            </div>
          </div>
        </div>
        <Headline
          videoUrl={headline.video.url}
          videoPoster={headline.video.image}
          hashTagAndLinks={headline.hashTagAndLinks}
          title={headline.section.title}
          industry={headline.section.industry}
          product={headline.section.product}
          size={headline.section.size}
          location={headline.section.location}
          year={headline.section.year}
          software={headline.section.software}
          todaysTechnology={headline.section.todaysTechnology}
          tommorowsVision={headline.section.tommorowsVision}
          trialBtnText={headline.section.trialBtn.text}
          trialBtnUrl={headline.section.trialBtn.url}
          quote={headline.quote.text}
          quotePersonName={headline.quote.who}
          quotePersonPosition={headline.quote.what}
        // quoteCompanyImg={companyImg}
        />
        <RelatedLinks title={podCast.title} podCastLinks={podCast.links} />
        <Carousel
          headline={carousel.title}
          carouselItems={carousel.items}
        />
      </div>
      </>
    );
  }
}

export default HighSchoolStudent;
