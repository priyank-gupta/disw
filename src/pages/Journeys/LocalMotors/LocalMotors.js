import React from "react";
import Headline from "../../../shared/Journeys/Headline/Headline";
import RelatedLinks from "../../../shared/Journeys/RelatedLinks/RelatedLinks";
import Carousel from "../../../shared/Journeys/Carousel/FourItemCarousel";
import companyImg from './img/local-motors-company.png';
import videoImg from './img/local-motors-video.png';
import video from './video/local-motors-video.mp4';
import { journeyCarouselTitle, journeyCarouselItems, relatedLinkIcon } from '../../../Constants';
import SocialShare from "../../../shared/SocialShare/SocialShare";

import SEOtags from "../../../SEOTags";
import twitterImage from "./img/industrial-additive-local-motors-siemens-1200X1200.jpg";
import ogImage from "./img/industrial-additive-local-motors-siemens-1200X630.jpg";

const LocalMotorsContent = {

  headTitle: {
    title: "Industrial Additive",
    lines: [
      "Realizing the dream of a 3-D printed automobile. ",
      "The success behind Local Motors with a little help from Siemens."],
  },

  headline: {
    video: {
      id: undefined,
      url: video,
      image: videoImg,
    },

    section: {
      title: { label: "Company", value: "Local Motors" },
      industry: { label: "Industry", value: { text: "Automotive", url: "https://www.plm.automation.siemens.com/global/en/industries/automotive-transportation/" } },
      product: { label: "Product", value: "3D Printing" },
      size: { label: "Size", value: "Small" },
      location: { label: "Location", value: "United States" },
      software: { label: "Siemens Software", value: { text: "Solid Edge", url: "https://solidedge.siemens.com/en/" } },
      todaysTechnology: { label: "Today's Technology", value: { text: "Additive Manufacturing", url: "https://www.plm.automation.siemens.com/global/en/products/manufacturing-planning/additive-manufacturing.html" } },
      tommorowsVision: { label: "Tomorrow's Vision", value: "Custom Manufacturing" },
      trialBtn: { text: "Try Solid Edge free", url: "https://www.plm.automation.siemens.com/store/en-us/trial/solid-edge.html", },
    },

    hashTagAndLinks: [
      {
        link: "https://twitter.com/search?q=%23todaymeetstomorrow", linkText: "#todaymeetstomorrow"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/industries/automotive-transportation/", linkText: "automotive"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/manufacturing-planning/additive-manufacturing.html", linkText: "additive manufacturing"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/manufacturing-planning/3d-printing.html", linkText: "3d printing"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/glossary/digital-twin/24465", linkText: "digital twin"
      },
      {
        link: "https://solidedge.siemens.com/en/", linkText: "solid edge"
      }
    ],

    quote: {
      text: 'What we’ve done with Siemens so far is just a start of what we can do. It will go well beyond software.',
      who: "Jay Rogers",
      what: "CEO, Local Motors",
      image: companyImg,
    },
  },

  podCast: {
    title: "Related Links",
    links: [
      {
        link: "https://new.siemens.com/global/en/company/stories/industry/local-motors-goes-global.html",
        linkText: "Local Motors goes global",
        podCastHeading: "Newsroom",
        icon: relatedLinkIcon["Newsroom"]
      },
      {
        link: "",
        linkText: "",
        podCastHeading: "Whitepaper",
        icon: relatedLinkIcon["Whitepaper"]
      },
      {
        link: "https://localmotors.com/2018/01/08/must-see-vehicle-ces-2018-accessibleolli/",
        linkText: "The Must-Experience Vehicle at CES 2018: #AccessibleOlli",
        podCastHeading: "Blogpost",
        icon: relatedLinkIcon["Blogpost"]
      },
      {
        link: "https://www.futuretechpodcast.com/podcasts/brittany-stotler-vp-marketing-local-motors/",
        linkText: "Brittany Stotler – VP of Marketing, Local Motors",
        podCastHeading: "Podcast",
        icon: relatedLinkIcon["Podcast"]
      }
    ],
  },

  carousel: {
    title: journeyCarouselTitle,
    items: journeyCarouselItems,
  },

};

class LocalMotors extends React.Component {
  render() {
    const { headTitle, headline, podCast, carousel } = LocalMotorsContent;
    return (
      <>
      <SEOtags
            seoTitle= "Industrial Additive"
            seoDescription= "With industrial additive manufacturing, Siemens helped a small automotive company realize the dream of a 3-D printed automobile using Solid Edge software."
            canonicalUrl="/journeys/industrial-additive/"
            twitterImage= {twitterImage}
            ogImage={ogImage}
      />
      <div>
        <div className="container">
          <div className="row mt-4">
            <div className="col">
              <SocialShare elm={<h1 className="pt-4 pb-3">{headTitle.title}</h1>}></SocialShare>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 pb-4">
              {
                headTitle.lines.map((subHeadLine, i) => {
                  return (
                    <h3 key={i}>{subHeadLine}</h3>
                  )
                })
              }
            </div>
          </div>
        </div>
        <Headline
          videoUrl={headline.video.url}
          videoPoster={headline.video.image}
          hashTagAndLinks={headline.hashTagAndLinks}
          title={headline.section.title}
          industry={headline.section.industry}
          product={headline.section.product}
          size={headline.section.size}
          location={headline.section.location}
          year={headline.section.year}
          software={headline.section.software}
          todaysTechnology={headline.section.todaysTechnology}
          tommorowsVision={headline.section.tommorowsVision}
          trialBtnText={headline.section.trialBtn.text}
          trialBtnUrl={headline.section.trialBtn.url}
          quote={headline.quote.text}
          quotePersonName={headline.quote.who}
          quotePersonPosition={headline.quote.what}
          quoteCompanyImg={headline.quote.image}
        />
        <RelatedLinks
          title={podCast.title}
          podCastLinks={podCast.links}
        />
        <Carousel
          headline={carousel.title}
          carouselItems={carousel.items}
        />
      </div>
      </>
    );
  }
}

export default LocalMotors;
