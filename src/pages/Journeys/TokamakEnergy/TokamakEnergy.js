import React from "react";
import Headline from "../../../shared/Journeys/Headline/Headline";
import RelatedLinks from "../../../shared/Journeys/RelatedLinks/RelatedLinks";
import Carousel from "../../../shared/Journeys/Carousel/FourItemCarousel";
import SocialShare from "../../../shared/SocialShare/SocialShare";

import companyImg from "./img/tokamak-energy-company.png";
import videoImg from "./img/tokamak-energy-video.jpg";
import video from "./video/tokamak-energy-video.mp4";
import { journeyCarouselTitle, journeyCarouselItems } from '../../../Constants';

import SEOtags from "../../../SEOTags";
import twitterImage from "./img/digital-twin-tokamak-energy-siemens-1200X1200.jpg";
import ogImage from "./img/digital-twin-tokamak-energy-siemens-1200X630.jpg";

const TokamakEnergyContent = {

  headTitle: {
    title: "Digital Twin",
    lines: [
      "Accelerating the development of fusion energy. ",
      "See how Siemens and Tokamak Energy are making it possible."
    ],
  },

  headline: {
    video: {
      id: undefined,
      url: video,
      image: videoImg,
    },

    section: {
      title: { label: "Company", value: "Tokamak Energy" },
      industry: { label: "Industry", value: { text: "Energy", href: "https://www.plm.automation.siemens.com/global/en/industries/energy-utilities/" } },
      product: { label: "Product", value: "Fusion Power" },
      size: { label: "Size", value: "Small" },
      location: { label: "Location", value: "United Kingdom" },
      software: { label: "Siemens Software", value: { text: "Teamcenter", href: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/" } },
      software2: { label: "Siemens Software", value: { text: "Solid Edge", href: "https://solidedge.siemens.com/en/" } },
      todaysTechnology: { label: "Today's Technology", value: { text: "Power Generation", href: "https://www.plm.automation.siemens.com/global/en/industries/energy-utilities/engineering-procurement-construction/" } },
      tommorowsVision: { label: "Tomorrow's Vision", value: "New Energy Alternatives" },
      trialBtn: { text: "Try Solid Edge free", url: "https://www.plm.automation.siemens.com/store/en-us/trial/solid-edge.html", },
    },

    hashTagAndLinks: [
      {
        link: "https://twitter.com/search?q=%23todaymeetstomorrow",
        linkText: "#todaymeetstomorrow"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/industries/energy-utilities/",
        linkText: "energy"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/customers/tokamak-energy/56719/",
        linkText: "fusion power"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/",
        linkText: "teamcenter"
      },
      {
        link: "https://solidedge.siemens.com/en/",
        linkText: "solid edge"
      }
    ],

    quote: {
      text: "Solid Edge gives us the accuracy we need, and as a new user I found it very intuitive. It is easy to learn through trial and error.",
      who: "Paul Tigwell",
      what: "Mechanical Design Consultant, Tokamak Energy",
      image: companyImg,
    },
  },

  podCast: {
    title: "Related Links",
    links: [
      {
        link:
          "http://www.eurekamagazine.co.uk/design-engineering-features/technology/time-to-engineer-fusion-energy/151121/",
        linkText: "Time to engineer fusion energy",
        podCastHeading: "Newsroom",
        icon: "book-open"
      },
      {
        link:
          "https://www.plm.automation.siemens.com/global/en/our-story/customers/tokamak-energy/56719/",
        linkText: "Tokamak Energy success story",
        podCastHeading: "Success Story",
        icon: "thumbs-up"
      },
      {
        link:
          "http://www.inspiring-sustainability.com/podcast/030-science-fiction-to-renewable-reality-nuclear-fusion-energy",
        linkText: "Science fiction to renewable reality: nuclear fusion energy",
        podCastHeading: "Podcast",
        icon: "podcast"
      }
    ],
  },

  carousel: {
    title: journeyCarouselTitle,
    items: journeyCarouselItems,
  },
};

class TokamakEnergy extends React.Component {
  render() {
    const { headTitle, headline, podCast, carousel } = TokamakEnergyContent;
    return (
      <>
       <SEOtags
            seoTitle= "Digital Twin"
            seoDescription= "To accelerate the development of fusion energy, a small energy company made fusion power generation possible with the help from Siemens and the digital twin."
            canonicalUrl="/journeys/digital-twin/"
            twitterImage= {twitterImage}
            ogImage={ogImage}
      />
      <div>
        <div className="container">
          <div className="row mt-4">
            <div className="col">
              <SocialShare elm={<h1 className="pt-4 pb-3">{headTitle.title}</h1>} />
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 pb-4">
              {headTitle.lines.map((subHeadLine, i) => {
                return <h3 key={i}>{subHeadLine}</h3>;
              })}
            </div>
          </div>
        </div>
        <Headline
          videoUrl={headline.video.url}
          videoPoster={headline.video.image}
          hashTagAndLinks={headline.hashTagAndLinks}
          title={headline.section.title}
          industry={headline.section.industry}
          product={headline.section.product}
          size={headline.section.size}
          location={headline.section.location}
          year={headline.section.year}
          software={headline.section.software}
          software2={headline.section.software2}
          todaysTechnology={headline.section.todaysTechnology}
          tommorowsVision={headline.section.tommorowsVision}
          trialBtnText={headline.section.trialBtn.text}
          trialBtnUrl={headline.section.trialBtn.url}
          quote={headline.quote.text}
          quotePersonName={headline.quote.who}
          quotePersonPosition={headline.quote.what}
          quoteCompanyImg={headline.quote.image}
        />
        <RelatedLinks
          title={podCast.title}
          podCastLinks={podCast.links}
        />
        <Carousel
          headline={carousel.title}
          carouselItems={carousel.items}
        />
      </div>
      </>
    );
  }
}

export default TokamakEnergy;
