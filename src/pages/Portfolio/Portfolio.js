import React from "react";
import Cards from "../../shared/Cards/Cards";
import Benefits from "../../shared/Cloud/benefits";
import SocialShare from "../../shared/SocialShare/SocialShare";
import image01 from "./img/xcelerator-portfolio.jpg";
import SEOtags from "../../SEOTags";
import twitterImage from "./img/Xcelerator-portfolio-siemens-1200X1200.jpg";
import ogImage from "./img/Xcelerator-portfolio-siemens-1200X630.jpg";

const title = "Discover the Xcelerator Portfolio";
const cardsContent = [
  {
    href: "/portfolio/cloud",
    title: "Cloud",
    desc:
      [
        "Realize unprecedented scalability and flexibility with Siemens Cloud Solutions – advanced cloud technology uniquely designed to help you take advantage of the cloud at your own pace."
      ]
  },
  {
    href: "https://www.mendix.com/",
    title: "Mendix",
    desc:
      [
        "Build what you can imagine – from enterprise-grade apps to simple tools – for increased user productivity."
      ]
  },
  {
    href: "https://www.mentor.com/",
    title: "Mentor",
    desc:
      [
        "Electronic design automation software allows you to develop more cost-effective electronic systems, components and products."
      ]
  },
  {
    href: "https://siemens.mindsphere.io/",
    title: "MindSphere",
    desc:
      [
        "An open IIoT operating system connecting products, plants, systems and machines. Harness the wealth of your data with advanced analytics."
      ]
  },
  {
    href: "https://www.plm.automation.siemens.com/global/en/",
    title: "Siemens PLM Software",
    desc:
      [
        "Successful manufacturers are using Siemens product lifecycle management software for innovation and growth."
      ]
  },
];

const content = {
  introduction: {
    title: "Xcelerator",
    subTitle: "The complete and fully integrated portfolio that spans industries and disciplines.",
    description: [
      "Xcelerator is our comprehensive and integrated portfolio of software and services that helps companies of all sizes become digital enterprises through a modern and open portfolio of technologies that integrates solutions for Product Lifecycle Management (PLM), Electronic Design Automation (EDA), Application Lifecycle Management (ALM), Manufacturing Operations Management (MOM), Embedded Software and Internet of Things (IoT).",
    ],
    welcomeMsg : "Welcome to Xcelerator: Where today meets tomorrow."
  },
  introImage: image01,
  benefits: {
    title: "Xcelerator Advantages",
    tiles: [
      {
        title: "Automation",
        description:
          "Supports the automation of your business from top floor to shop floor.",
        icon: "far fa-laptop-code"
      },
      {
        title: "Innovation",
        description:
          "Melds model-based simulations with test data and real performance analytics for innovation and validation.",
        icon: "far fa-sparkles"
      },
      {
        title: "Digital thread",
        description:
          "Orchestrates the flow of information across the ecosystem by creating a traceable digital thread.",
        icon: "far fa-wave-square"
      },
    ],
  },
  xceleratorCont: {
    subTitle: "The Comprehensive Digital Twin",
    subs: [
      {
        sub2Description: "The digital twin is a precise virtual model of a product, or production assets, which evolves throughout the lifecycle. A digital twin is used to predict behavior, optimize performance and apply lessons learned from previous design and production experiences. Using the digital twin as a verification/validation tool is essential for today’s complex products.",
      },
      {
        sub2Description: "Xcelerator gives our customers an edge in the market. It provides unique insights and data which allows users to adapt these complex products quickly; merging the virtual and real worlds in software, hardware, design and manufacturing for the most comprehensive digital twin. It also offers the most open and flexible ecosystem in industrial software.",
      },
    ],
  },
};


class Portfolio extends React.Component {
  render() {
    return (
      <>
      <SEOtags
            seoTitle= "Xcelerator Portfolio"
            seoDescription= "Xcelerator uses the low-code application development platform and app services for IoT to allow businesses of all sizes to become digital enterprises."
            canonicalUrl="/portfolio/"
            twitterImage= {twitterImage}
            ogImage={ogImage}
      />
      {/* introduction: H1 tag */}
      <div className="container">{/** py-5 */}
        <div className="row">
          <div className="col">
            <div className="d-flex my-4"> {/** my-5 */}
              <div className={"m-auto d-none d-lg-block w-75"}>
                <SocialShare
                  elm={<h1 className="pt-4 pb-3">{content.introduction.title}</h1>}
                />
                <h3 className="pb-2">{content.introduction.subTitle}</h3>
                <p>{content.introduction.description}</p>
                <p className="font-weight-bold pb-0"><em>{content.introduction.welcomeMsg}</em></p>
              </div>
              <div className="m-auto pb-4 d-block d-lg-none">
                <SocialShare
                  elm={<h1 className="pt-4 pb-4">{content.introduction.title}</h1>}
                />
                <h3 className="pb-2">{content.introduction.subTitle}</h3>
                <p>{content.introduction.description}</p>
                <p className="font-weight-bold pb-0"><em>{content.introduction.welcomeMsg}</em></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* reuse cloud benifit */}
      <Benefits
        benefitsTile={content.benefits.tiles}
        socialShare={"no"}
      />
      {/* image */}
      <div className="container mt-5">
        <div className={"row"}>
          <div className={"w-75 ml-auto d-none d-md-block mr-auto"}>
            <img src={content.introImage} alt="" className="px-3 pt-5 ml-auto mr-auto img-fluid" id="image1"  />
          </div>
          {/* mobile image view*/}
          <div className={"ml-auto mr-auto d-block d-md-none"}>
            <img src={content.introImage} alt="" className="px-3 pt-5 ml-auto mr-auto img-fluid" id="image1"  />
          </div>
        </div>
      </div>
      {/* comprehensive digital twin section */}
      <div className="container py-2">
        <div className="row">
          <div className="col">
            <div className="d-flex my-2">
              <div className={"m-auto d-none d-lg-block w-75"}>
                <h2 className="pt-3 pb-3">{content.xceleratorCont.subTitle}</h2>
              </div>
              <div className="w-100 m-auto pb-4 d-block d-lg-none">
                <h2 className="pt-3 pb-3">{content.xceleratorCont.subTitle}</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* digital twin paragraph one */}
      <div className="container">
        <div className="row">
          <div className="col">
            <div className="d-flex">
              <div className={"m-auto d-none d-lg-block w-75"}>
                <p>{content.xceleratorCont.subs[0].sub2Description}</p>
              </div>
              <div className="m-auto pb-4 d-block d-lg-none w-100">
                <p>{content.xceleratorCont.subs[0].sub2Description}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* digital twin paragraph two */}
      <div className="container">
        <div className="row">
          <div className="col">
            <div className="d-flex">
              <div className={"m-auto d-none d-lg-block w-75"}>
                <p>{content.xceleratorCont.subs[1].sub2Description}</p>
              </div>
              <div className="m-auto pb-4 d-block d-lg-none">
                <p>{content.xceleratorCont.subs[1].sub2Description}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* xcelerator portfolio section*/}
      <div className="container mt-5">
        <h2 className="pt-5 pb-5">{title}</h2>
      </div>
      <Cards cardsContent={cardsContent}/>
    </>
    );
  }
}

export default Portfolio;
