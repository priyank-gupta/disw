import React from "react";
import Benefits from "../../../shared/Cloud/benefits";
import SocialShare from "../../../shared/SocialShare/SocialShare";
import image01 from "./img/xcelerator-application-development-platform.jpg";
import image02 from "./img/digital-twin.jpg";

const content = {
  introduction: {
    title: "Xcelerator – Embrace the Digital Future",
    description: [
      "Xcelerator is a comprehensive and integrated portfolio of software and services for electronic and mechanical design, system simulation, manufacturing, operations and lifecycle analytics. It can be personalized and adapted to fit customer and industry-specific needs – and easily extended using the powerful Mendix low-code application development platform.",
    ]
  },
  introImage: image01,
  introAfter: {
    title: "The Software Foundation for the Digital Enterprise",
    description: "Xcelerator helps companies of all sizes become digital enterprises through a modern and open portfolio of technologies that integrates solutions for Product Lifecycle Management, Electronic Design Automation (EDA), Application Lifecycle Management (ALM), Manufacturing Operations Management (MOM), Embedded Software and Internet of Things (IoT).",
  },
  benefits: {
    title: "Xcelerator Advantages",
    tiles: [
      {
        title: "Automation",
        description:
          "Supports the automation of your business from top floor to shop floor",
        icon: "far fa-laptop-code"
      },
      {
        title: "Innovation",
        description:
          "Melds model-based simulations with test data and real performance analytics for innovation and validation",
        icon: "far fa-sparkles"
      },
      {
        title: "Digital thread",
        description:
          "Orchestrates the flow of information across the ecosystem by creating a traceable digital thread",
        icon: "far fa-wave-square"
      },
    ],
  },

  xceleratorCont: {
    title: "Mendix Powers the Xcelerator Ecosystem",
    description: "Following the acquisition of Mendix, the global leader in low-code and no-code application development for the enterprise, the Mendix platform has expanded to include app services for IoT powered by MindSphere, digital engineering and system integrations. The Mendix multi-experience application platform enables anyone in the ecosystem, including citizen developers and engineers, to easily build, integrate and extend existing data and systems.",

    subTitle: "The Comprehensive Digital Twin",
    subDescription: "",

    subs: [
      {
        sub2Title: "Precise Model of Each and Every Phase",
        sub2Description: "The digital twin is a precise virtual model of a product, or production assets, which evolves throughout the lifecycle. A digital twin is used to predict behavior, optimize performance and apply lessons learned from previous design and production experiences. Using the digital twin as a verification/validation tool is essential for today’s complex products and smart production systems.",
        sub2Image: image02,
      },
      {
        sub2Title: "Connecting with Smart Connected Products",
        sub2Description: "The digital twin captures physical asset performance data from products and plants in operation. Data from smart connected products in the field and factory equipment is aggregated, analyzed and transformed into actionable information to create a completely closed-loop decision environment for continuous optimization.",
        sub2Image: undefined,
      },
    ],
  },

};

class Xcelerator extends React.Component {

  render() {

    return (
      <>
        {/* introduction: H1 tag */}
        <div className="container">{/** py-5 */}
          <div className="row">
            <div className="col">
              <div className="d-flex my-4"> {/** my-5 */}
                <div className={"m-auto d-none d-md-block w-75"}>
                  <SocialShare
                    elm={<h1 className="pt-4 pb-4">{content.introduction.title}</h1>}
                  />
                  <p>{content.introduction.description}</p>
                </div>
                <div className="m-auto pb-4 d-block d-md-none">
                  <SocialShare
                    elm={<h1 className="pt-4 pb-4">{content.introduction.title}</h1>}
                  />
                  <p>{content.introduction.description}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* image */}
        <div className="container mb-4 ">
          <div className={"row"}>
            <div className={"w-75 ml-auto mr-auto"}>
              <img src={content.introImage} alt="" className="px-3 ml-auto mr-auto img-fluid" id="image1"  />
            </div>
          </div>
        </div>
        {/* introduction after */}
        <div className="container">{/** py-5 */}
          <div className="row">
            <div className="col">
              <div className="d-flex my-4"> {/** my-5 */}
                <div className={"m-auto d-none d-md-block w-75"}>
                  <h2 className="pt-4 pb-4">{content.introAfter.title}</h2>
                  <p>{content.introAfter.description}</p>
                </div>
                <div className="m-auto pb-4 d-block d-md-none">
                  <h2 className="pt-4 pb-4">{content.introAfter.title}</h2>
                  <p>{content.introAfter.description}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* reuse cloud benifit */}
        <Benefits
          title={content.benefits.title}
          benefitsTile={content.benefits.tiles}
          socialShare={"no"}
        />
        {/* after benefits h2 segment */}
        <div className="container py-3">
          <div className="row">
            <div className="col">
              <div className="d-flex my-3">
                <div className={"m-auto d-none d-md-block w-75"}>
                  <h2 className="pt-4 pb-4">{content.xceleratorCont.title}</h2>
                  <p>{content.xceleratorCont.description}</p>
                </div>
                <div className="m-auto pb-4 d-block d-md-none">
                  <h2 className="pt-4 pb-4">{content.xceleratorCont.title}</h2>
                  <p>{content.xceleratorCont.description}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* h3 segment after h2*/}
        <div className="container py-2">
          <div className="row">
            <div className="col">
              <div className="d-flex my-2">
                <div className={"m-auto d-none d-md-block w-75"}>
                  <h3 className="pt-3 pb-3">{content.xceleratorCont.subTitle}</h3>
                </div>
                <div className="m-auto pb-4 d-block d-md-none">
                  <h3 className="pt-3 pb-3">{content.xceleratorCont.subTitle}</h3>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* h4 segment after h3*/}
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="d-flex">
                <div className={"m-auto d-none d-md-block w-75"}>
                  <h4 className="pt-1 pb-3">{content.xceleratorCont.subs[0].sub2Title}</h4>
                  <p>{content.xceleratorCont.subs[0].sub2Description}</p>
                </div>
                <div className="m-auto pb-4 d-block d-md-none">
                  <h4 className="pt-1 pb-3">{content.xceleratorCont.subs[0].sub2Title}</h4>
                  <p>{content.xceleratorCont.subs[0].sub2Description}</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container">
          <div className={"row mt-5 mb-5 "}>
            <div className={"w-75 ml-auto mr-auto"} >
              <img src={content.xceleratorCont.subs[0].sub2Image} alt="" className="px-3 ml-auto mr-auto img-fluid" id="image1"  />
            </div>
          </div>
        </div>

        {/* h4 segment after h3*/}
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="d-flex">
                <div className={"m-auto d-none d-md-block w-75"}>
                  <h4 className="pt-1 pb-3">{content.xceleratorCont.subs[1].sub2Title}</h4>
                  <p>{content.xceleratorCont.subs[1].sub2Description}</p>
                </div>
                <div className="m-auto pb-4 d-block d-md-none">
                  <h4 className="pt-1 pb-3">{content.xceleratorCont.subs[1].sub2Title}</h4>
                  <p>{content.xceleratorCont.subs[1].sub2Description}</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row mb-5">
          </div>
        </div>
      </>
    );
  }
}

export default Xcelerator;
