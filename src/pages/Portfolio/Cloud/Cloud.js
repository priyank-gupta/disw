import React from "react";
import Introduction from "../../../shared/Cloud/introduction";
import Benifit from "../../../shared/Cloud/benefits";
import Video from "../../../shared/Video/Video";
import Headline from "../../../shared/Journeys/Headline/Headline";
import CloudTypes from "../../../shared/Cloud/cloudTypes";
import styles from "./Cloud.module.scss";
import "../../../index.scss";

import cloudVideoPoster from "./img/Cloud-Solution-Video-Poster.png";
import cloudVideo from "./video/Video-Siemens-Cloud-Solutions.mp4";
import companyImg from "../../Journeys/AmericaMakes/img/america-makes-company.png";
import journeyVideoImg from "../../Journeys/AmericaMakes/img/america-makes-video.png";
import journeyVideo from "../../Journeys/AmericaMakes/video/america-makes-video.mp4";
import SaaSIndSolImage from "./img/saas-is1023232352-cloudsb-400x250.jpg";
import IoTAppsImage from "./img/i-o-t-is1051788710-cloudsb-400x250.jpg";
import DevPltImage from "./img/dev-is1088107102-cloudsb-400x250.jpg";
import CloudReadyImage from "./img/cloud-ready-is995757216-cloudsb-400x250.jpg";

import SEOtags from "../../../SEOTags";
import twitterImage from "./img/cloud-siemens-1200X1200.jpg";
import ogImage from "./img/cloud-siemens-1200X630.jpg";

const textArray = ["Flexible", "Scalable", "Secure",];
const CloudContent = {

  introTitle: "Siemens Cloud Solutions: Your Way, Your Pace ",
  description: [
    "Embracing cloud-based solutions is no longer enough; businesses must find new paths for growth and innovation. By connecting systems and processes, we are empowering companies with superior technology and industry insight.",
    "With a unique understanding of customer needs, unprecedented access to domain services expertise and the most flexible and scalable cloud solutions on the market, our customers receive an enhanced overall cloud experience – not just in performance – but in evaluation, purchasing, on-boarding and support."
  ],

  benefitsTitle: "Benefits of Cloud Computing in Business",
  benefitsTile: [
    {
      title: "Secure",
      description:
        "Best-in-class cloud management and business solutions provide maximum security for customers' peace of mind and digitalization opportunities.",
      icon: "far fa-lock-alt"
    },
    {
      title: "Flexible",
      description:
        "Cloud integration strategies offer adaptable and customized transformations to the cloud based on the timeframes and methods our customers choose.",
      icon: "far fa-shapes"
    },
    {
      title: "Scalable",
      description:
        "Operational benefits of cloud include reducing time-to-market and connecting distributed, cross-disciplinary teams while improving effectiveness and efficiency at any scale.",
      icon: "far fa-user-chart"
    },
    {
      title: "Simple",
      description:
        "Cloud managed PLM provides seamless integrations and operations. Our cloud software portfolio resolves complex issues with sophisticated yet unintimidating cloud technology.",
      icon: "far fa-tasks"
    },
    {
      title: "Affordable",
      description:
        "Economic benefits of the cloud translate to new revenue streams. Small businesses can grow while maintaining the bottom line, while large enterprises can build and extend cloud investments.",
      icon: "far fa-hands-usd"
    },
    {
      title: "Innovative",
      description:
        "Digitalization in cloud computing helps drive business forward with both speed and agility while continuing to maintain the highest of quality standards.",
      icon: "far fa-sparkles"
    }
  ],

  cloudSolutionHeadlineTitle: "Siemens Cloud Solutions for Your Business",
  cloudSolutionHeadlineDetail: "Watch to learn more about the benefits associated with implementing the best cloud managed operations at your organization.",

  cloudSolutionSection: "SOLUTIONS",
  cloudSolutionTitle: "Siemens Cloud Computing Solutions",
  cloudSolution: [
    {
      title: "Cloud Ready",
      description: "Execute multiple deployment models of cloud computing with a cloud connected desktop, using our existing products on the cloud. Experience an on-demand ability to scale up resources utilizing the infrastructure you prefer, along with SaaS and our Cloud Managed Services.",
      image: CloudReadyImage,
      link: "https://www.plm.automation.siemens.com/global/en/our-story/cloud/cloud-ready.html"
    },
    {
      title: "Development Platform",
      description: "Build apps to meet your specific needs using our multi-experience application development platform that includes app services for digital engineering, IoT, integration and core needs – all consumed through low-code.",
      image: DevPltImage,
      link: "https://www.plm.automation.siemens.com/global/en/topic/cloud-application-platform/58148"
    },
    {
      title: "SaaS Industry Apps",
      description: "Access our flexible, purpose-built cloud native apps anywhere, anytime. Small and medium businesses can now scale, collaborate and enhance productivity across all teams, suppliers and customers with cloud manufacturing and cloud engineering solutions.",
      image: SaaSIndSolImage,
      link: "https://www.plm.automation.siemens.com/global/en/our-story/cloud/saas-apps-is.html"
    },
    {
      title: "IoT Apps",
      description: "Connect to business insights and new customers with our Internet of Things (IoT) capabilities, featuring powerful industry solutions and microservices supported by advanced data analytics. Our IoT cloud applications allow you to scale on a global basis. ",
      image: IoTAppsImage,
      link: "https://siemens.mindsphere.io/en"
    },
  ],

  customerJourneyHeadlineTitle: "Cloud Customer Spotlight",
  customerJourneyHeadlineDetail: "Learn how cloud computing helps companies utilize cloud-based software to scale and innovate. See how America Makes created their Digital Storefront for Additive Manufacturing by using PLM on the cloud.",

  customerJourney: {
    video: {
      id: undefined,
      url: journeyVideo,
      image: journeyVideoImg,
    },

    section: {
      title: { label: "Company", value: "America Makes" },
      industry: { label: "Industry", value: { text: "Manufacturing", url: "https://www.plm.automation.siemens.com/global/en/products/manufacturing-planning/" } },
      product: { label: "Product", value: "Digital Storefront" },
      size: { label: "Size", value: "Small" },
      location: { label: "Location", value: "United States" },
      software: { label: "Siemens Software", value: { text: "Teamcenter", url: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/" } },
      todaysTechnology: { label: "Today's Technology", value: { text: "Additive Manufacturing", url: "https://www.plm.automation.siemens.com/global/en/products/manufacturing-planning/additive-manufacturing.html" } },
      tommorowsVision: { label: "Tomorrow's Vision", value: "Industrial Additive" },
      trialBtn: { text: "Teamcenter", url: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/" },
    },

    hashTagAndLinks: [
      {
        link: "https://twitter.com/search?q=%23todaymeetstomorrow ",
        linkText: "#todaymeetstomorrow"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/glossary/digital-manufacturing/13157",
        linkText: "digital manufacturing"
      },
      {
        link: "https://www.americamakes.us/our_work/#Work_DSF",
        linkText: "digital storefront"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/our-story/glossary/industrial-internet-of-things/57242",
        linkText: "IIoT"
      },
      {
        link: "https://www.plm.automation.siemens.com/global/en/products/teamcenter/",
        linkText: "teamcenter"
      }
    ],

    quote: {
      text: "Siemens has and continues to be a great partner to us from a sales, education, support and services perspective.",
      who: "Joe Veranese",
      what: "Business Systems Director, The National Center for Defense Manufacturing and Machining",
      image: companyImg
    },
  },
};

class Cloud extends React.Component {

  constructor(props) {
    super(props);
    this.state = { textIdx: 0 };
  }

  componentDidMount() {
    this.timeout = setInterval(() => {
      let currentIdx = this.state.textIdx;
      this.setState({ textIdx: currentIdx + 1 });
    }, 3000);
  }

  componentWillUnmount() {

    clearInterval(this.timeout);
  }

  render() {
    let textThatChanges = textArray[this.state.textIdx % textArray.length];
    const cont = CloudContent,
      headline = cont.customerJourney;

    return (
      <>
       <SEOtags
            seoTitle= "Siemens Cloud Solutions"
            seoDescription= "Siemens Cloud Solutions offers advanced cloud technology and cloud computing with scalability and flexibility to take advantage of cloud benefits at your pace."
            canonicalUrl="/portfolio/cloud/"
            twitterImage= {twitterImage}
            ogImage={ogImage}
      />
      <div>
        <section id="cloudSolutionsHeroImg" className={styles.cloudSolutionImg}>
          <div className={styles.cloudSolutionOverLay + " d-flex"}>
            <div className="d-flex m-auto">
              <h1 className="m-auto text-center">
                <span className="font-weight-bold color_ffb903">{textThatChanges}</span>{" "}
                <span className="font-weight-bold text-white">with Siemens Cloud Solutions</span>
              </h1>
            </div>
          </div>
        </section>
        <Introduction
          title={cont.introTitle}
          description={cont.description}
        />
        <Benifit
          title={cont.benefitsTitle}
          benefitsTile={cont.benefitsTile}
        />
        <section id="videoSectionCloudSolution" className="py-5">
          <div className="container">
            <div className="row">
              <div id="cloudSolutionsHeadlineAndPar" className="w-75 d-none d-lg-block">
                <div className="col-12">
                  <h2>{cont.cloudSolutionHeadlineTitle}</h2>
                  <p className="text-muted mb-4">{cont.cloudSolutionHeadlineDetail}</p>
                </div>
              </div>
              <div id="cloudSolutionsHeadlineAndPar" className="d-block d-lg-none">
                <div className="col-12">
                  <h2>{cont.cloudSolutionHeadlineTitle}</h2>
                  <p className="text-muted mb-4">{cont.cloudSolutionHeadlineDetail}</p>
                </div>
              </div>
              <div className="col-12">
                <Video videoPoster={cloudVideoPoster} videoUrl={cloudVideo} />
              </div>
            </div>
          </div>
        </section>
        {/* Solution Chapter */}
        <CloudTypes
          sectionTitle={cont.cloudSolutionSection}
          title={cont.cloudSolutionTitle}
          cloudSolution={cont.cloudSolution}
        />
        <section id="customerJourneyWrapper" className="py-5">
          <div className="container">
            <div className="row">
              <div id="customerJourneyHeadlineWrapper" className="w-75 d-none d-lg-block">
                <div className="col-12">
                  <h2>{cont.customerJourneyHeadlineTitle}</h2>
                  <p className="text-muted mb-4">{cont.customerJourneyHeadlineDetail}</p>
                </div>
              </div>

              <div id="customerJourneyHeadlineWrapper" className="d-block d-lg-none">
                <div className="col-12">
                  <h2>{cont.customerJourneyHeadlineTitle}</h2>
                  <p className="text-muted mb-4">{cont.customerJourneyHeadlineDetail}</p>
                </div>
              </div>

            </div>
          </div>
          <Headline
            videoUrl={headline.video.url}
            videoPoster={headline.video.image}
            hashTagAndLinks={headline.hashTagAndLinks}
            title={headline.section.title}
            industry={headline.section.industry}
            product={headline.section.product}
            size={headline.section.size}
            location={headline.section.location}
            year={headline.section.year}
            software={headline.section.software}
            todaysTechnology={headline.section.todaysTechnology}
            tommorowsVision={headline.section.tommorowsVision}
            trialBtnText={headline.section.trialBtn.text}
            trialBtnUrl={headline.section.trialBtn.url}
            quote={headline.quote.text}
            quotePersonName={headline.quote.who}
            quotePersonPosition={headline.quote.what}
            quoteCompanyImg={headline.quote.image}
          />
        </section>
      </div>
      </>
    );
  }
}

export default Cloud;
