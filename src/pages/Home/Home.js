import React from "react";
import ReactDOM from "react-dom";
import SimpleLink from "../../shared/lib/SimpleLink";
import Introduction from "../../shared/Cloud/introduction";
import Benifit from "../../shared/Cloud/benefits";
import ThreeTiles from "../../shared/Journeys/Tiles/ThreeTiles";
import styles from "./Home.module.scss";

import { journeyCarouselItems } from "../../Constants";
import video from "./video/siemens-digital-industries-software-video.mp4";

import SEOtags from "../../SEOTags";
import twitterImage from "./img/siemens-ingenuity-for-life-1200X1200.jpg";
import ogImage from "./img/siemens-ingenuity-for-life-1200X630.jpg";

const HomepageContent = {

  textArray: ["engineering", "manufacturing", "design", "today"],

  todaymeetstomo: {
    href: "https://twitter.com/search?q=%23todaymeetstomorrow",
    text: "#TodayMeetsTomorrow",
    className: styles["btn"] + " text-white"
  },

  introTitle: "Welcome to Siemens Digital Industries Software",
  introDescr: [
    "Formerly Siemens PLM Software, our new name reflects the depth of our software offerings across a broad spectrum of industry domains.",
    "Amid unprecedented change and the rapid pace of innovation, digitalization is no longer tomorrow’s idea. We take what the future promises tomorrow and make it real for our customers today.",
    "Welcome to… Where today meets tomorrow."
  ],

  journeyTitle: "The promise of tomorrow here today with these customer journeys",
  journeyDescription: "By blurring the boundaries between industry domains – across both physical and digital worlds – we bring the technologies of tomorrow to our customers today. These are their stories.",

  benefitTitle: "Why Siemens?",
  benefitsTile: [
    {
      title: "Holistic digital twin",
      description:
        "We blur the boundaries between industry domains by integrating the virtual and physical, hardware and software, design and manufacturing worlds. Our comprehensive digital twin enables a complete digital transformation in discrete and process industries.",
      icon: "far fa-clone"
    },
    {
      title: "Personal and adaptable",
      description:
        "We offer flexible and scalable applications for new ways of working. With insights and data, we can predict and adapt products to future needs, allowing you the ability to meet rapidly changing consumer preferences.",
      icon: "far fa-arrows"
    },
    {
      title: "Open, modern, flexible ecosystem",
      description:
        "Access to a diverse field of technology partners in an open ecosystem gives you the opportunity to build on your investment. We offer rapid application development services along with native cloud and cloud-connected products.",
      icon: "far fa-laptop-code"
    },
  ],

};

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = { textIdx: 0 };
  }

  componentDidMount() {
    this.timeout = setInterval(() => {
      let currentIdx = this.state.textIdx;
      this.setState({ textIdx: currentIdx + 1 });
    }, 3000);

    const hpv = ReactDOM.findDOMNode(this.refs.homepageVideo);
    hpv.setAttribute("muted", true);
  }

  componentWillUnmount() {
    clearInterval(this.timeout);
  }

  render() {
    const cont = HomepageContent;
    let textThatChanges = cont.textArray[this.state.textIdx % cont.textArray.length];

    return (
      <>
        <SEOtags
            seoTitle= ""
            seoDescription= "Formerly Siemens PLM Software, our new name Siemens Digital Industries Software reflects the depth of our software offerings across industry domains."
            canonicalUrl=""
            twitterImage= {twitterImage}
            ogImage={ogImage}
        />
        <div className={styles["header"]}>
          <div className={styles.overLay}>
            <video ref="homepageVideo"
              className={styles["video"]}
              playsInline={true}
              width="auto"
              height="auto"
              autoPlay={true}
              muted={true}
              loop={true}
            >
              <source src={video} type="video/mp4" />
            </video>
            <div
              className={
                "container " +
                styles["container"] +
                " " +
                styles["desktop-only"]
              }
            >
              <div className={styles["flex-left"]}>
                <div className={styles["flex-item-center"]}>
                  <div
                    className={
                      "col-sm-12 " + styles["green-overlay-background"]
                    }
                  >
                    <span className={styles["spnHeading"]}>
                      Where{" "}
                      <span className={styles["secondary-text"]}>
                        {textThatChanges}
                      </span>{" "}
                      meets tomorrow
                    </span>
                  </div>
                </div>
              </div>
              <div className={styles["flex-center-bottom"]}>
                <div className={styles["flex-item-center"]}>
                  <SimpleLink link={cont.todaymeetstomo} />
                </div>
              </div>
            </div>
            <div
              className={
                "container " + styles["container"] + " " + styles["mobile-only"]
              }
            >
              <div className={styles["flex-left"]}>
                <div className={styles["flex-item-top"]}>
                  <div
                    className={
                      "col-sm-12 " +
                      styles["green-overlay-background"]
                    }
                  >
                    <span className={styles["spnHeading"]}>
                      Where{" "}
                      <span className={styles["secondary-text"]}>
                        {textThatChanges}
                      </span>{" "}
                      meets tomorrow
                    </span>
                  </div>
                </div>
              </div>
              <div className={styles["flex-center-bottom-mobile"]}>
                <div className={styles["flex-item-center-mobile"]}>
                  <SimpleLink link={cont.todaymeetstomo} />
                </div>
              </div>
            </div>
          </div>
        </div>
        <Introduction
          headTag={"h1"}
          title={cont.introTitle}
          description={cont.introDescr}
        />
        <ThreeTiles titleTag={"h2"} title={cont.journeyTitle} description={cont.journeyDescription} tiles={journeyCarouselItems} />
        <Benifit
          title={cont.benefitTitle}
          benefitsTile={cont.benefitsTile}
        />
        
      </>
    );
  }
}

export default Home;
