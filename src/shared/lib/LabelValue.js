import React from "react";
import SimpleLink from "./SimpleLink";
// <LabelValue labelValue0={labelValue0} labelValue1={labelValue1}/>
// labelValue0 and labelValue1 are obj like {label, value}, which label is string, value is string or SimpleLink link obj 
// if no labelValue0, it returns empty string; labelValue1 is optional
class LabelValue extends React.Component {
  render() {
    const labelValue0 = this.props.labelValue0;
    if (!labelValue0) {
      return "";
    }
    const labelValue1 = this.props.labelValue1,
      label = labelValue0.label,
      value = labelValue0.value,
      value2 = (labelValue1 && labelValue1.value) ? labelValue1.value : undefined;
    return (value ?
      (value2 ?
        <p className="mb-0">
          <span className="font-weight-bold mr-1">{label + ": "}</span>
          <SimpleLink link={value} className="text-dark-orange" />,&nbsp;
          <SimpleLink link={value2} className="text-dark-orange" />
        </p> :
        <p className="mb-0">
          <span className="font-weight-bold mr-1">{label + ": "}</span>
          <SimpleLink link={value} className="text-dark-orange" />
        </p>) : ""
    );
  };
};

export default LabelValue;
