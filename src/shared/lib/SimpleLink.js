import React from "react";
// <SimpleLink link={link} className={} newTab=true|false key={key}/>
// link is obj like {link, text, className} or string, no anchor link if string 
// className is optional
// newTab is optional 
// key is optional 
class SimpleLink extends React.Component {
  render() {
    const lnk = this.props.link, key = this.props.key;
    if (!lnk || typeof lnk === "string") {
      return (<>{lnk ? lnk : ""}</>);
    }
    const href = (lnk.href || lnk.link || lnk.url || "#"),
      text = (lnk.text || lnk.linkText),
      clazz = (this.props.className || lnk.className),
      x = href.indexOf(window.location.host),
      newTab = this.props.newTab || x === -1;
    if (newTab) {
      return (
        <>
          {clazz ?
            <a key={key ? key : undefined} className={clazz} href={href} target="_blank" rel="noopener noreferrer">{text}</a>
            : <a key={key ? key : undefined} href={href} target="_blank" rel="noopener noreferrer">{text}</a>
          }
        </>
      );
    }
    return (
      <>
        {clazz ?
          <a key={key ? key : undefined} className={clazz} href={href}>{text}</a>
          : <a key={key ? key : undefined} href={href}>{text}</a>
        }
      </>
    );
  }
}

export default SimpleLink;
