
function hasClass(el, className, classNameRegExp) {
  const regex = classNameRegExp || new RegExp("(\\s|^)" + className + "(\\s|$)");
  if (el.classList) return el.classList.contains(className);
  return !!el.className.match(regex);
}

function addClass(el, className) {
  if (el.classList) el.classList.add(className);
  else {
    const regex = new RegExp("(\\s|^)" + className + "(\\s|$)");
    if (!hasClass(el, className, regex)) el.className += " " + className;
  }
}

function removeClass(el, className) {
  if (el.classList) el.classList.remove(className);
  else {
    const regex = new RegExp("(\\s|^)" + className + "(\\s|$)");
    if (hasClass(el, className, regex)) {
      el.className = el.className.replace(regex, " ");
    }
  }
}

function addScript(srcUrl, type) {
  let script = document.createElement("script");
  script.type = type || "text/javascript";
  script.src = srcUrl;
  document.getElementsByTagName('body')[0].appendChild(script);
}

function isSelfHref(href) {
  if (!href || href.length < 2) return true;
  if (href.charAt(0) === "/" && href.charAt(1) !== "/") return true;
  const x = href.indexOf(window.location.host);
  return x > 0;
}

function isIE() {
  // return !!window.MSInputMethodContext && !!document.documentMode;
  return !(window.ActiveXObject) && "ActiveXObject" in window;
}

export default {
  addScript: addScript,
  isSelfHref: isSelfHref,
  isIE: isIE,
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass
};
