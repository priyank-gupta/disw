import React from "react";
// TryButton is an anchor link.
// <TryButton url={url} text={text} dataSteComponent={dataSteComponent}/>
// url is link string 
// text is string
// dataSteComponent is string, optional
// optional attibutes: anchorCss, textCss, iconCss
class TryButton extends React.Component {
  render() {
    const url = this.props.url || this.props.link || "#", 
      text = this.props.text || this.props.buttonText,
      dataSteComponent = this.props.dataSteComponent;
    const aCss = this.props.anchorCss || "btn btn-gradient-orange mt-4 text-center d-flex align-items-center justify-content-center w_20rem",
      textCss = this.props.textCss || "mr-2 fontSize_1_3",
      iconCss = this.props.iconCss || "fal fa-long-arrow-right fa-2x";
    return (
      <a
        className={aCss}
        href={url}
        data-ste-component={dataSteComponent ? dataSteComponent : undefined}
        target="_blank" rel="noopener noreferrer"
      >
        <span className={textCss}>{text}</span>
        <i className={iconCss} />
      </a>
    );
  }
}

export default TryButton;
