import React from 'react';
import styles from "./Carousel.module.scss";
import { Link, NavLink } from 'react-router-dom';

// Slick Carousel
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import Slider from "react-slick";
import $ from 'jquery'; 

class FourItemCarousel extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      shouldRender : false
    };
  }

  componentDidMount() {
    this.setState({
      shouldRender: true
    })
  }


  // When a carousel item is clicked, scroll to the top of the page in 500ms.
  scrollToTop = () => function() {
    window.scrollTo(0,0);
  };

  render() {
        // Left arrow for the Carousel in order to set module classes
        const ArrowLeft = (props) => (
            <button {...props} className={ 'slick-arrow slick-prev ' + styles['slick-arrow'] + ' ' + styles['slick-prev'] } />
        );

        // Right arrow for the carousel in order to set module classes
        const ArrowRight = (props) => (
            <button {...props} className={ 'slick-arrow slick-next ' + styles['slick-arrow'] + ' ' + styles['slick-next'] } />
        );

        // Slick carousel settings.
        const settings = {
          dots: false,
          className: styles['slick-slider'],
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 1,
          initialSlide: 0,
          arrows: true,
          prevArrow: <ArrowLeft />,
          nextArrow: <ArrowRight />,
          responsive: [
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
              }
            }
          ]
        };
        
        return ( this.state.shouldRender ?
          <section className='py-5'>
            {/* <!-- Top content --> */}
            <div className="container">
                
              <div className="row pb-5 text-center text-md-left">
                <div className="col">
                  <h3>{this.props.headline}</h3>
                </div>
              </div>

              <div className="top-content text-center">
                <Slider {...settings}> {
                  this.props.carouselItems.map((obj, i)=>{
                    return (
                      <>
                        <NavLink to={obj.link} activeClassName={ 'active' } onClick={ this.scrollToTop() }>
                          <i className={ `fal ${ obj.icon } fa-5x color-5 ` }> </i>
                          <p className="my-4 text-uppercase">{obj.linkText}</p>
                        </NavLink>
                      </>
                    );
                  }) }
                </Slider>
              </div>
            </div>
          </section> : <></>
        );
    }
}

export default FourItemCarousel;