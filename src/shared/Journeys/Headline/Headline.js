import React from "react";
import styles from "./Headline.module.scss";
import Video from "../../Video/Video";
import SimpleLink from "../../lib/SimpleLink";
import LabelValue from "../../lib/LabelValue";
import TryButton from "../../lib/TryButton";
import "../../../index.scss";

class Headline extends React.Component {
  renderHashTagsAndLinks = (hashTagAndLinks) => {
    return hashTagAndLinks.map((obj, i) => {
      const lastIndex = hashTagAndLinks.length - 1,
        cname = (i !== lastIndex) ?
          styles.hashTagsLinksBorder + " border-light-orange px-2 d-inline-block fontSize_1_0625" :
          "pl-2 d-inline-block fontSize_1_0625";
      return (
        <SimpleLink link={obj} className={cname} />
      );
    });
  };

  renderCustomerInfo = () => {
    return (
      <div id="content-right-of-video-section" className="col-lg-4 mt-3 mt-lg-0">
        <span className="text-uppercase font-weight-bold color_2387a9 fontSize_1_08">
          {this.props.title.label}
        </span>
        <p className={"mb-0 font-weight-bold " + styles.companyBorder + " fontSize_1_87"}>
          {this.props.title.value}
        </p>
        <div id="company-stats" className="mt-3 mb-2">
          <LabelValue labelValue0={this.props.industry} />
          <LabelValue labelValue0={this.props.product} />
          <LabelValue labelValue0={this.props.size} />
          <LabelValue labelValue0={this.props.location} />
          <LabelValue labelValue0={this.props.year} />
        </div>
        <div id="siemens-software">
          <LabelValue labelValue0={this.props.software} labelValue1={this.props.software2} />
          <LabelValue labelValue0={this.props.todaysTechnology} />
          <LabelValue labelValue0={this.props.tommorowsVision} />
        </div>
        <TryButton text={this.props.trialBtnText} url={this.props.trialBtnUrl} dataSteComponent={"conversion-main"} />
      </div>
    );
  };

  renderQuote = (quote, who, position, quoteImg) => {
    return (
      <div className="d-flex">
        <div className={"w-75 m-auto"}>
          <div className="d-flex justify-content-between">
            <i className="fas fa-quote-left fa-3x pr-3 d-none d-md-block color_ffb903" />
            <div id="quote-wrapper" className="d-flex flex-column" style={{flex: 1}}>
              <p className={"font-italic " + styles.quote_font_size}>{quote}</p>
              <p className="mb-0 align-self-end text-right text-uppercase w-100">
                <span className="font-weight-bold">{who + ",  "}</span>
                {position}
              </p>
              <img
                className="img-fluid align-self-end"
                src={quoteImg} alt=""
              />
            </div>
            <i className="fas fa-quote-right fa-3x pl-3 d-none d-md-block color_ffb903" />
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <section>
        <div className="container">
          <div className="row mb-5">
            {/* video and Tags */}
            <div id="video-section" className="col-lg-8">
              <Video
                videoPoster={this.props.videoPoster}
                videoUrl={this.props.videoUrl}
              />
              <div className="my-3">{this.renderHashTagsAndLinks(this.props.hashTagAndLinks)}</div>
            </div>
            {/* Customer Info */}
            {this.renderCustomerInfo()}
          </div>
          {/* Quote */}
          <div className="row py-5">
            <div className="col-12">
              {this.renderQuote(this.props.quote,
                this.props.quotePersonName,
                this.props.quotePersonPosition,
                this.props.quoteCompanyImg)}
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Headline;
