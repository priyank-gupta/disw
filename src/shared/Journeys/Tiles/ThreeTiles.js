import React from "react";
import { Link, NavLink } from 'react-router-dom';
import styles from "./Tiles.module.scss";
import imageBottom from "../../../pages/Home/img/journey-media-list-bottom.svg";
import imageTop from "../../../pages/Home/img/journey-media-list-top.svg";

class ThreeTiles extends React.Component {
  // When a item is clicked, scroll to the top of the page in 500ms.
  scrollToTop = () => function() {
    window.scrollTo(0,0);
  };

  render() {
    const TitleTag = this.props.titleTag || "h3";
    return (
      <section>
        <div className="container">

          {/* <!-- IMG top --> */}  
          <div className="row text-right pb-3 d-none d-md-block">
            <div className="col">
              <img src={imageTop} alt=""/>
            </div>
          </div>

          {/* <!-- Heading & Description --> */}  
          <div className="row">
            <div className="col-lg-9 col-12">
              <TitleTag className="pb-3">{this.props.title}</TitleTag>
              <p>{this.props.description}</p>
            </div>
          </div>

          {/* <!-- Tiles for desktop and tablet views --> */} 
          <div className={"row py-3 m-auto w-75 " + styles.journeyTile + " d-none d-md-flex"}>
            {this.props.tiles.map((obj, i)=>{
              return (
                <div className={"col-md-6 col-lg-4 p-4 d-flex "}>
                  <div className={"row w-100 " + styles.border}>
                    <div className="col-12 p-0 pb-3">
                      <Link to={obj.link} onClick={ this.scrollToTop() }><h4><strong>{obj.company}</strong></h4>
                        <h5>{obj.linkText}</h5>
                      </Link>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
          {/* <!-- Tiles for mobile views --> */} 
          <div className={"row py-3 m-auto d-flex d-md-none " + styles.journeyTile}>
            {this.props.tiles.map((obj, i)=>{
              return (
                <div className="col-12 py-3 pl-0">
                  <Link to={obj.link} onClick={ this.scrollToTop() }>
                    <div className={"d-flex flex-row justify-content-between pb-3 " + styles.border}>
                      <div className="align-self-left">
                        <h4><strong>{obj.company}</strong></h4>
                        <h5>{obj.linkText}</h5>
                      </div>
                      <div className="align-self-center">
                        <i className="fal fa-long-arrow-right fa-2x"></i>
                      </div>
                    </div>
                  </Link>
                </div>
              );
            })}
          </div>

          {/* <!-- IMG bottom --> */}  
            <div className="row text-left d-none d-md-block">
              <div className="col mb-5">
                <img src={imageBottom} alt=""/>
              </div>
            </div>
          </div>
        </section>
      );
    }
}

export default ThreeTiles;