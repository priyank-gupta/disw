import React from "react";
import styles from "./RelatedLinks.module.scss";
import "../../../index.scss";

class RelatedLinks extends React.Component {

  renderRelatedLinksData = () => {
    return <>
      <div className={styles.pb}>
        <h2 className="pb-4">{this.props.title}</h2>
      </div>
      <div id="relatedLinks">
        {this.props.podCastLinks.map((obj, i) => {
          return (
            obj.link !== "" && (
              <div className={`row py-3 ${styles.bottomBorder}`}>
                <div className={`col-sm-3 ${styles.alignIcon}`}>
                  <i className={`fas fa-${obj.icon} ${styles.faColor} w-25 d-none d-md-inline-block`} />
                  <i className={`fas fa-${obj.icon} ${styles.faColor} d-inline-block d-md-none w_10`} />
                  <span className={styles.bodyText + " "}>{obj.podCastHeading}&nbsp;&nbsp;</span>
                  <i className="fal fa-long-arrow-right" />
                </div>
                <div className={"col-sm-9 " + styles["col-sm-9"]}>
                  <a target="_blank" rel="noopener noreferrer" href={obj.link}>{obj.linkText}</a>
                </div>
              </div>
            )
          );
        })}
      </div>
    </>
  }

  render() {
    return (
      <div className={styles.bgcolor}>
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="m-auto w-75 d-none d-lg-block">
                { this.renderRelatedLinksData() }
              </div>
              <div className="d-block d-lg-none">
                { this.renderRelatedLinksData() }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RelatedLinks;
