import React from "react";
import styles from "./Video.module.scss";

class Video extends React.Component {
  constructor(props) {
    super(props);
    this.videoRef = React.createRef();
  }

  state = {
    videoClicked: false
  };

  playVideo = () => {
    this.setState({ videoClicked: true });
    this.videoRef.current.setAttribute("controls", "controls");
    this.videoRef.current.play();
  };

  render() {
    const videoIdIndex = this.props.videoUrl.lastIndexOf("/") + 1,
    videoId = this.props.videoUrl.substring(videoIdIndex);
    return (
      <div
        className={
          "embed-responsive embed-responsive-16by9 " + styles.videoWrapper
        }
      >
        <video
          ref={this.videoRef}
          className="embed-responsive-item"
          poster={this.props.videoPoster}
          id={videoId ? videoId : undefined}
        >
          <source src={this.props.videoUrl} type="video/mp4" />
        </video>
        <i
          onClick={this.playVideo}
          className={ styles.fontSizeIcon + 
            " fal fa-play-circle justify-content-center align-items-center " +
            styles.playPause +
            " " +
            (this.state.videoClicked ? "d-none" : "d-flex")
          }
        />
      </div>
    );
  }
}

export default Video;
