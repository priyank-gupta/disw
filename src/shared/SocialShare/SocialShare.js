import React from "react";
import styles from "./SocialShare.module.scss";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  FacebookIcon,
  TwitterIcon,
  LinkedinIcon
} from "react-share";

class SocialShare extends React.Component {
  render() {
    return (
      <div className="d-inline-block">
        <div className="d-flex justify-content-start align-items-center">
          {this.props.elm}
          <div className="p-3">
            <div className="d-flex  align-items-center">
              <a
                data-toggle="collapse"
                href="#collapseDesktop"
                className="mr-2 d-none d-lg-inline"
              >
                <i className="far fa-share-alt fa-2x text-muted" />
              </a>
              <a
                data-toggle="collapse"
                href="#collapseMobile"
                className="mr-2 d-inline d-lg-none"
              >
                <i className="far fa-share-alt fa-2x text-muted" />
              </a>
              <div
                id="collapse-wrapper-desktop"
                className={"d-none d-lg-block " + styles.collapseDesktopWrapper}
              >
                <div
                  className={"collapse " + styles.collapseDesktop}
                  id="collapseDesktop"
                >
                  <div className="card">
                    <div
                      className={
                        "card-body d-flex justify-content-between bg-light " +
                        styles.cardBodyPadding
                      }
                    >
                      <FacebookShareButton
                        className={"mr-2 " + styles.shareButton}
                        url={window.location.href}
                      >
                        <FacebookIcon size={32} round />
                      </FacebookShareButton>
                      <LinkedinShareButton
                        className={"mr-2 " + styles.shareButton}
                        url={window.location.href}
                      >
                        <LinkedinIcon size={32} round />
                      </LinkedinShareButton>
                      <TwitterShareButton
                        className={"mr-2 " + styles.shareButton}
                        url={window.location.href}
                      >
                        <TwitterIcon size={32} round />
                      </TwitterShareButton>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="collapse-wrapper-mobile" className="d-lg-none mb-4">
          <div className="collapse" id="collapseMobile">
            <div className="d-flex justify-content-end">
              <div className="card d-inline-block">
                <div className="card-body d-flex bg-light">
                  <FacebookShareButton
                    className={"mr-2 " + styles.shareButton}
                    url={window.location.href}
                  >
                    <FacebookIcon size={32} round />
                  </FacebookShareButton>
                  <LinkedinShareButton
                    className={"mr-2 " + styles.shareButton}
                    url={window.location.href}
                  >
                    <LinkedinIcon size={32} round />
                  </LinkedinShareButton>
                  <TwitterShareButton
                    className={"mr-2 " + styles.shareButton}
                    url={window.location.href}
                  >
                    <TwitterIcon size={32} round />
                  </TwitterShareButton>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SocialShare;
