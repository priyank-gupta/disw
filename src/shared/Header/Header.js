import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { BrowserRouter, Route } from "react-router-dom";

import styles  from './Header.module.scss';
import logo from './../../logo.svg';
import { navigation } from '../../Constants';

// Header class rendering out the navigation pulled from Constants.js
class Header extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      navigationItems : navigation,
      desktopNavigationExpanded : false, // desktop navigation open state handled independent of mobile
      mobileNavigationExpanded : false   // mobile navigation open state
    };
  }

  // When a user hovers over the subnavigation items, first loop through all of the other drop-downs and 
  // set their display to false. Finally, set the display property of the hovered element to true.
  onHoverSubnavigation (element, childElement) {
    element.dropdownItems.map((item, index) => { item.displayPanel = false; });
    childElement.displayPanel = true;
    this.setState(this.state);
  }

  // Fade the background when the nav drop-down is open
  fadeBackground() {
    var bgOverlay = document.getElementById('bg-overlay');
    bgOverlay.style.height = "100%"; bgOverlay.style.opacity = "1";
  }

  // Show the background when the nav is closed (remove the dark overlay)
  showBackground() {
    var bgOverlay = document.getElementById('bg-overlay');
    bgOverlay.style.height = "0"; bgOverlay.style.opacity = "0";
  }

  // Open the drop-down element 
  toggleDropdownMobile(element) {
    element.isExpandedMobile = !element.isExpandedMobile;
    this.setState(this.state);
  }

  // Close the drop-downs by looping through the navigation and marking it as not-expanded
  closeDesktopDropdowns() { 
    this.state.navigationItems.map((item, index) => { item.isExpandedDesktop = false; });
  }

  // Close the mobile drop-downs (mobile only)
  closeMobileDropdowns() { 
    this.state.navigationItems.map((item, index) => { item.isExpandedMobile = false; });
    this.setState({ mobileNavigationExpanded : false }); 
  }

  // Toggle the drop-downs (desktop)
  toggleDesktopDropdown(element) { 
    this.state.navigationItems.filter((item) => item.link !== element.link).map((item, index) => { item.isExpandedDesktop = false; });
    element.isExpandedDesktop = !element.isExpandedDesktop;
    this.setState(this.state); 
  }

  // Toggle the mobile drop-down (toggle the dark overlay and flip whether or not the mobile nav is expanded)
  toggleMobileNavigation() {
    var bgOverlay = document.getElementById('bg-overlay');
    if(bgOverlay.style.height === "100%") { bgOverlay.style.height = "0%"; bgOverlay.style.opacity = "0"; } 
    else { bgOverlay.style.height = "100%"; bgOverlay.style.opacity = "1"; }
    this.setState({ mobileNavigationExpanded : !this.state.mobileNavigationExpanded }); 
  }

  // Render the mobile overview link (so the top-level links are clickable)
  renderMobileOverviewLink = (item) => {
    if(item.hasLink !== false) {
      return <NavLink to={item.link} exact className="d-xs-block d-lg-none" activeClassName="font-weight-bold" onClick={() => { this.toggleMobileNavigation(); this.closeDesktopDropdowns(); this.closeMobileDropdowns(); }}>
        <li><span><i className="fa fa-angle-right d-md-none pr-3 "></i> Overview</span> </li> 
      </NavLink>
    }
  }
  
  // Check whether or not the current path should set the item as being active in the navigation.
  checkActiveState = (path) => {
    return window.location.pathname.includes(path) || window.location.pathname === path ? styles.active : '';
  }

  // Render the top-level navigation items which have no drop-down items. 
  // NOTE that several links are rendered; one per device, so that the user actions could be handled differently (i.e. a click on a top-level item
  // on desktop changes the route and links the user, whereas a click on mobile expands the navigation).
  renderTopLevelNavigationWithNoDropdowns = (item) => {
    return <>
      {/* Mobile top-level navigation item (no drop-down) */}
      <NavLink to={ item.hasLink === false ? '#' : item.link } className={'d-xs-block d-md-none nav-link ' + styles['nav-link'] + ' ' + this.checkActiveState(item.link)} 
          onClick={() => { this.closeDesktopDropdowns(); this.closeMobileDropdowns(); this.toggleMobileNavigation(); this.showBackground(); }} >
        <span>{item.title}</span>
      </NavLink>

      {/* Tablet top-level navigation item (no drop-down) */}
      <NavLink to={ item.hasLink === false ? '#' : item.link } className={'d-none d-md-block d-lg-none nav-link ' + styles['nav-link'] + ' ' + this.checkActiveState(item.link)} 
          onClick={() => { this.closeDesktopDropdowns(); this.closeMobileDropdowns(); this.toggleMobileNavigation(); this.showBackground(); }} >
        <span>{item.title}</span>
      </NavLink>

      {/* Desktop top-level navigation item (no drop-down) */}
      <NavLink to={ item.hasLink === false ? '#' : item.link } className={'d-none d-lg-block nav-link ' + styles['nav-link'] + ' ' + this.checkActiveState(item.link)}
          onClick={() => { this.closeDesktopDropdowns(); this.closeMobileDropdowns(); this.toggleMobileNavigation(); this.showBackground(); }}
          onMouseEnter={() => { this.closeDesktopDropdowns(); this.closeMobileDropdowns(); this.showBackground(); }} >
        <span>{item.title}</span>
      </NavLink>
    </>
  }

  // Render the top-level navigation items which have drop-downs. See note above on function 'renderTopLevelNavigationWithNoDropdowns' about the repeated nav link blocks.
  renderTopLevelNavigationWithDropdowns = (item) => {
    return <>
      {/* MOBILE - we need to handle the events different on mobile so show this block on XS only */}
      <NavLink to={ item.hasLink === false ? '#' : item.link } onClick={(event) => { event.preventDefault(); this.closeDesktopDropdowns(); this.toggleDropdownMobile(item); this.fadeBackground(); }}
          className={'d-xs-block d-md-none nav-link dropdown-toggle ' + styles['nav-link'] + ' ' + styles['dropdown-toggle'] + ' ' + (item.isExpandedDesktop ? 'mb-0' : '') + ' ' + this.checkActiveState(item.link)} >
        <span>{item.title} <i className={'pl-1 pb-1 fa ' + (item.isExpandedDesktop || item.isExpandedMobile ? 'fa-caret-up' : 'fa-caret-down')} ></i></span>
      </NavLink>

      {/* Tablet - we need to handle the events different on mobile so show this block on XS only */}
      <NavLink to={ item.hasLink === false ? '#' : item.link } onClick={(event) => { event.preventDefault(); this.closeDesktopDropdowns(); this.toggleDesktopDropdown(item); this.fadeBackground(); }}
          className={'d-none d-md-block d-lg-none nav-link dropdown-toggle ' + styles['nav-link'] + ' ' + styles['dropdown-toggle'] + ' ' + (item.isExpandedDesktop ? 'mb-0' : '') + ' ' + this.checkActiveState(item.link)} >
        <span>{item.title} <i className={'pl-1 pb-1 fa ' + (item.isExpandedDesktop || item.isExpandedMobile ? 'fa-caret-up' : 'fa-caret-down')} ></i></span>
      </NavLink>

      {/* DESKTOP - similarly we need to handle mouse enter events on desktop (instead of click) so show them block on SM+ */}
      <NavLink to={ item.hasLink === false ? '#' : item.link } onClick={() => { this.setState(this.state) }} onMouseEnter={() => { this.closeDesktopDropdowns(); this.toggleDesktopDropdown(item); this.fadeBackground(); }}
          className={'d-none d-lg-block nav-link dropdown-toggle ' + styles['nav-link'] + ' ' + styles['dropdown-toggle'] + ' ' + (item.isExpandedDesktop ? 'mb-0' : '') + ' ' + (this.checkActiveState(item.link))} >
        <span>{item.title} <i className={'pl-1 fa ' + (item.isExpandedDesktop || item.isExpandedMobile ? 'fa-caret-up' : 'fa-caret-down')} ></i></span>
      </NavLink>
    </>
  }

  

  render() {

    // Render out the list of link elements (including drop-downs) from the JSON structure
    const items = this.state.navigationItems.map((item, index) => { 
      return item.dropdownItems === undefined || item.dropdownItems.length < 1 ? 
        
        // Top-level navigation items (No drop-down)
        <li className={'nav-item ' + styles['nav-item']}>
          { this.renderTopLevelNavigationWithNoDropdowns(item) }
        </li> :

        // Top-level drop-down navigation items (parent is still clickable but drop-down will expand)
        <li className={'position-static dropdown nav-item ' + styles['dropdown'] + ' ' + styles['nav-item']}>

          {/* Render the top-level navigation which have drop-downs */}
          { this.renderTopLevelNavigationWithDropdowns(item) }

          {/* Drop-down menu (show if it's set to expanded in the JSON) */}
          <div className={'w-100 dropdown-menu ' + styles['dropdown-menu'] + ' ' + (item.isExpandedDesktop || item.isExpandedMobile ? 'show' : '')} >
            <div className="container"> 
              <div className="row">

                {/* Render out the list elements (left-pane of dropdown) */}
                <div className={'dropdown-panel panel-left col-md-4 p-0 ' + styles['dropdown-panel'] + ' ' + styles['panel-left']}>
                  <ul>
                    {/* Mobile & Tablet - display a link to the drop-down item itself (clicking opens the dropdown versus on desktop, where the top level item is linkable) */}
                    { this.renderMobileOverviewLink(item) }
                    
                    {/* Render out the drop-down items rendering the external URLs differently. */}
                    {item.dropdownItems.map((subItem, innerIndex) => { return !subItem.link.startsWith("/") ?
                      <a href={subItem.link} target="_blank" rel="noopener">
                        <li onMouseEnter={() => this.onHoverSubnavigation(item, subItem)}> <span><i className="fa fa-angle-right d-md-none pr-3 "></i> {subItem.title}</span> </li> 
                      </a> :
                      <NavLink to={subItem.link} exact activeClassName="font-weight-bold" onClick={() => { this.toggleMobileNavigation(); this.closeDesktopDropdowns(); this.closeMobileDropdowns(); }}>
                        <li onMouseEnter={() => this.onHoverSubnavigation(item, subItem)}> <span><i className="fa fa-angle-right d-md-none pr-3 "></i> {subItem.title}</span> </li> 
                      </NavLink>
                    })} 
                  </ul>
                </div>
                
                {/* Render out the drop-down panel (right-pane of dropdown) */}
                {item.dropdownItems.map((subItem, innerIndex) =>
                  <div className={'col-md-5 pl-5 d-none dropdown-panel panel-right ' + styles['dropdown-panel'] + ' ' + styles['panel-right'] + ' ' + (subItem.displayPanel == true ? 'd-lg-block' : '')}>
                    <h5 className="font-weight-bold">{subItem.title}</h5>
                    <p>{subItem.description}</p>
                  </div>
                )}
              </div>
            </div>
          </div>
        </li>
    });

    return (
        <nav className={ styles['navbar'] + ' navbar navbar-expand-md navbar-light'} onMouseLeave={() => { this.closeDesktopDropdowns(); this.closeMobileDropdowns(); this.showBackground(); } } >
        
        <div className="container"> 
          {/* Logo */}
          <NavLink className={styles['navbar-brand'] + ' navbar-brand'} to="/"><img src={logo} /></NavLink>

          {/* Mobile Navigation Toggle */}
          <button className={'navbar-toggler background-color-7 ' + styles['navbar-toggler']} type="button" onClick={() => this.toggleMobileNavigation()}  >
            { !this.state.mobileNavigationExpanded ? <i className="fa fa-bars"></i> : '' }
            {  this.state.mobileNavigationExpanded ? <i className="fa fa-times"></i> : '' }
          </button>

          {/* Navigation including drop-downs */}
          <div className={'collapse navbar-collapse ' + (this.state.mobileNavigationExpanded ? 'show' : '')}>
            <ul className={styles['navbar-nav'] + ' navbar-nav mr-auto'}>
              
              {/* DISW tagline (mobile only) */}
              <span className={'float-right ' + styles['site-title'] + ' site-title d-xs-block d-md-none py-3'}>Siemens Digital Industries Software</span>

              {/* Render out the list of navigation items */}
              {items}
            </ul>

              {/* DISW tagline (desktop only) */}
            <span className={'float-right ' + styles['site-title'] + ' site-title d-none d-lg-block '}>Siemens Digital Industries Software</span>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;