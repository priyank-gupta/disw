import React from 'react';
class SectionTitle extends React.Component {

    render(){
        return(
            <span className="text-uppercase font-weight-bold" style={{color: '#2387A9', fontSize: '1.08rem'}}>
                {this.props.sectionTitle}
            </span>
        );
    }

}

export default SectionTitle;