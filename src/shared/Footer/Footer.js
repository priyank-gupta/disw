import React from 'react';
import style from "./footer.module.scss";
import homestyle from "../../pages/Home/Home.module.scss";

const FooterContent = {
   copyright1 : "©2019 Siemens Product Lifecycle Management",
   copyright2 : " Software Inc. All Rights Reserved.",
   term : {
    href: "https://new.siemens.com/global/en/general/terms-of-use.html",
    text: "TERMS OF USE"
  },
   privacy : {
    href: "https://www.plm.automation.siemens.com/global/en/legal/privacy-policy.html",
    text: "PRIVACY POLICY"
  },
   cookie : {
    href: "https://new.siemens.com/global/en/general/cookie-notice.html",
    text: "COOKIE POLICY"
  },
};

class Footer extends React.Component {

  render() {
    const cont = FooterContent;
    return (
      <div className={style.bottom_footer}>
        {/* desktop */}
        <div className={"container " + homestyle["desktop-only"]}>
          <div className={"row"}>
            <div className="col-md-6">
              <span className="cc">{cont.copyright1 + cont.copyright2}</span>
            </div>
            <div className={"col-md-6 text-nowrap text-right " + style["policy-box"]}>
              <div className={style.policy}>
                <span className={style.tou}>
                  <a href={cont.term.href} rel="noopener noreferrer" target="_blank">{cont.term.text}</a></span>
                <span className={style.privacy}>
                  <a href={cont.privacy.href} rel="noopener noreferrer" target="_blank">{cont.privacy.text}</a></span>
                <span className={style.cookie}>
                  <a href={cont.cookie.href} rel="noopener noreferrer" target="_blank">{cont.cookie.text}</a></span>
              </div>
            </div>
          </div>
        </div>
        {/* mobile */}
        <div className={"container " + homestyle["mobile-only"]}>
          <div className={"row"}>
            <div className="col-md-12 text-nowrap text-center">
              <span className="cc">{cont.copyright1}</span>
            </div>
          </div>
          <div className={"row"}>
            <div className="col-md-12 text-nowrap text-center">
              <span className="cc">{cont.copyright2}</span>
            </div>
          </div>
          <div className={"row"}>
            <div className="col-md-12">&nbsp;</div>
          </div>
          <div className={"row"}>
            <div className="col-md-12 text-nowrap text-center">
              <span className={style.tou}>
                <a href={cont.term.href} rel="noopener noreferrer" target="_blank">{cont.term.text}</a></span>
              <span className={style.privacy}>
                <a href={cont.privacy.href} rel="noopener noreferrer" target="_blank">{cont.privacy.text}</a></span>
              <span className={style.cookie}>
                <a href={cont.cookie.href} rel="noopener noreferrer" target="_blank">{cont.cookie.text}</a></span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
