import React from "react";
import style from "./cards.module.scss";
import SocialShare from "../SocialShare/SocialShare";
import "./cardsIE.scss";
import slib from "../lib/shareLib.js";

// cards style with template
// <div class="container">
//  <TitleTag class=title}>{title}</TitleTag> <!-- optional -->
//  <h3 class=subTitle}>{subTitle}</h3> <!-- optional -->
//  <div class=cardsDiv>
//   <div id="cards" class="cards<N>"> <!-- N integer: 2<N<10 -->
//
//     <!-- can have multiple anchor a tags-->
//     <a class=cardAnch>
//       <span class=cardDesc>
//         <h3 class=cardDescTitle></h3>
//         <p class=cardDescDetail></p> <!-- can have multiple p tags optional -->
//       </span>
//       <span class=cardLinkIconSpan> <!-- optional -->
//         <i class=cardLinkIcon></i> <!-- optional -->
//       </span>
//     </a>
//
//   </div>
//  </div>
// </div>
const cardsStyle = {
  title: style.cards_title, // cards title
  subTitle: style.cards_subtitle, // cards subtitle
  cardsDiv: "portfolio",
  cardAnch: "card " + style.card,
  cardDesc: style.desc,
  cardDescTitle: style.desc_title,
  cardDescDetail: style.desc_detail,
  cardLinkIconSpan: style.desc_linkicon,
  cardLinkIcon: "fal fa-long-arrow-right " + style.fal,
};

class Cards extends React.Component {
  render() {

    document.addEventListener("DOMContentLoaded", function (event) {
      var ua = window.navigator.userAgent,
        msie = ua.indexOf("MSIE"),
        cards = [].slice.call(document.querySelectorAll("a.card")),
        csize = cards.length;
      if (msie === -1) msie = ua.indexOf(".NET");
      if (msie > 0) {
        var width = window.innerWidth
          || document.documentElement.clientWidth
          || document.body.clientWidth,
          col = 1, row = 0;
        if (width > 860) {
          for (let i = 0; i < csize; i++) {
            if (i % 3 === 0) row++;
            if (col === 4) col = 1;
            let e = cards[i],
              cname = "card_r" + row + "c" + col;
            slib.addClass(e, cname);
            col++;
          }
        }
        else if (860 >= width && width > 576) {
          for (let i = 0; i < csize; i++) {
            if (i % 2 === 0) row++;
            if (col === 3) col = 1;
            let e = cards[i],
              cname = "card_r" + row + "c" + col;
            slib.addClass(e, cname);
            col++;
          }
        }
        else {
          for (let i = 0; i < csize; i++) {
            row = i + 1;
            let e = cards[i],
              cname = "card_r" + row + "c1";
            slib.addClass(e, cname);
          }
        }
      } else {
        cards.forEach(function (c) {
          slib.removeClass(c, "card_r1c1");
          slib.removeClass(c, "card_r1c2");
          slib.removeClass(c, "card_r1c3");
          slib.removeClass(c, "card_r2c1");
          slib.removeClass(c, "card_r2c2");
          slib.removeClass(c, "card_r2c3");
          slib.removeClass(c, "card_r3c1");
          slib.removeClass(c, "card_r3c2");
          slib.removeClass(c, "card_r3c3");

          slib.removeClass(c, "card_r4c1");
          slib.removeClass(c, "card_r4c2");
          slib.removeClass(c, "card_r5c1");
          slib.removeClass(c, "card_r5c2");
          slib.removeClass(c, "card_r6c1");
          slib.removeClass(c, "card_r7c1");
          slib.removeClass(c, "card_r8c1");
          slib.removeClass(c, "card_r9c1");
        });
      }
    });

    const title = this.props.title, subTitle = this.props.subTitle, socialShare = this.props.socialShare,
      TitleTag = this.props.titleTag || "h1", cards = this.props.cardsContent;
    const css = this.props.cardStyle || cardsStyle;

    return (
      <div className="container">
        {title && socialShare &&
          <SocialShare
            elm={<TitleTag className={css.title}>{title}</TitleTag>}
          />
        }
        {title && !socialShare &&
          <TitleTag className={css.title}>{title}</TitleTag>
        }
        {subTitle && (subTitle.indexOf("</a>") === -1 ?
          (<h3 className={css.subTitle}>
            {subTitle}
          </h3>) :
          (<h3 className={css.subTitle}>
            Our robust <a href='/portfolio/' className="color_2387a9">Xcelerator portfolio</a> spans across industries and covers a spectrum of disciplines.
          </h3>)
        )}
        <div className={css.cardsDiv}>
          <div id="cards" className={style["cards" + cards.length]}>
            {cards.map((card, i) => {
              return (
                <a className={css.cardAnch} href={card.href} target={slib.isSelfHref(card.href) ? "_self" : "_blank"} rel={slib.isSelfHref(card.href) ? "" : "noopener"}>
                  <span className={css.cardDesc}>
                    <h3 className={css.cardDescTitle}>{card.title}</h3>
                    {card.desc.map((desc, i) => {
                      return <p className={css.cardDescDetail}>{desc}</p>;
                    })}
                  </span>
                  {css.cardLinkIcon &&
                    <span className={css.cardLinkIconSpan}>
                      <i className={css.cardLinkIcon} />
                    </span>
                  }
                </a>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default Cards;
