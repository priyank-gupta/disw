import React from "react";
import style from "./benefits.module.scss";

class Benefits extends React.Component {
  render() {
    return (
      <div className="container pt-5">
        {this.props.title &&
        <div className="row">
          <div className="col text-center text-lg-left">
            <h2 className={style.benefitTitle}>{this.props.title}</h2>
          </div>
          
        </div>
        }
        <div className="row">
          {this.props.benefitsTile.map((obj, i) => {
            return (
              <div className={'col-12 col-lg-4 offset-lg-0 ' + (this.props.benefitsTile.length % 2 === 0 ? 'col-sm-6' : 'col-sm-10 offset-sm-1') } >
                <div className={style.card + ' d-flex flex-row'}>
                    <i className={obj.icon + "  " + style.fal + " d-inline"} />
                    <h4 className='font-weight-bold'>{obj.title}</h4>
                </div>
                <div className="pb-5"><p className={style.tile_description}>{obj.description}</p></div>
              </div> 
            );
          })}
        </div>
      </div>
    );
  }
}

export default Benefits;
