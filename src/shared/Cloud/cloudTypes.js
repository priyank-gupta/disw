import React from "react";
import style from "./cloudTypes.module.scss";
import CloudTile from "./CloudTile";

class CloudTypes extends React.Component {
  render() {
    return (
      <section id="cloudTypesWrapper" className="py-5">
        <div className="container">
          <div className="d-flex">
            <div className={"m-auto d-none d-sm-none d-md-block " + style["width"]}>
              <div className={style["cloudTypes-header"]}>
                <div className="container">
                  <p className={"mb-0 font-weight-bold " + style.solutionTitle}>
                    {this.props.sectionTitle}
                  </p>
                  <div className="title">
                    <h2>
                      {this.props.title}
                    </h2>
                  </div>
                </div>
              </div>
              <div className="container">
                {this.props.cloudSolution.map((obj, i) => {
                  return (
                    <CloudTile cloudTypeSolutions={obj} index={i} />
                  );
                })}
              </div>
            </div>
            <div className="d-block d-sm-block d-md-none">
              <div className={style["cloudTypes-header"]}>
                <div className="container p-0">
                  <p className={"mb-0 font-weight-bold " + style.solutionTitle}>
                    {this.props.sectionTitle}
                  </p>
                  <div className="title">
                    <h2>
                      {this.props.title}
                    </h2>
                  </div>
                </div>
              </div>
              <div className="container p-0">
                {this.props.cloudSolution.map((obj, i) => {
                  return (
                    <a href={obj.link} target="_blank" rel="noopener noreferrer">
                      <div className={style["cloudTypes-tile"]} >
                        <img className={style["img"]} alt="" src={obj.image} />
                        <div className={style.header}>
                          <h3 className="mt-4"><b>{obj.title}</b></h3>
                          <p>{obj.description}</p>
                        </div>
                        <b><span className={"text-center d-flex align-items-center " + style.icon}>
                          CONTINUE READING &nbsp;
                        <i className={"fal fa-long-arrow-right fa-2x"}></i>
                        </span></b>
                      </div>
                    </a>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default CloudTypes;
