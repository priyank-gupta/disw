import React from "react";
import SocialShare from "../SocialShare/SocialShare";
import style from "./introduction.module.scss";

class Introduction extends React.Component {
  render() {
    const HeadTag = this.props.headTag || "h2",
      socialShare = this.props.socialShare || "yes",
      title = this.props.title, descs = this.props.description;
    return (
      <div className="container py-5">
        <div className="row">
          <div className="col">
            <div className="d-flex my-5">
              <div className={"m-auto d-none d-md-block w-75"}>
                {socialShare === "yes" ? 
                (<SocialShare
                  elm={<HeadTag className="pt-4 pb-4">{title}</HeadTag>}
                />) : (<HeadTag className="pt-4 pb-4">{title}</HeadTag>)
                }
                {Array.isArray(descs) ? descs.map((desc, i) => {
                  return <p>{desc}</p>;
                }) : (<p>{descs}</p>)}
              </div>

              <div className="m-auto pb-4 d-block d-md-none">
                {socialShare === "yes" ? 
                (<SocialShare
                  elm={<HeadTag className="pt-4 pb-4">{title}</HeadTag>}
                />) : (<HeadTag className="pt-4 pb-4">{title}</HeadTag>)
                }
                {Array.isArray(descs) ? descs.map((desc, i) => {
                  return <p>{desc}</p>;
                }) : (<p>{descs}</p>)}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Introduction;