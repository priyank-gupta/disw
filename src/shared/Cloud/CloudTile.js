import React from "react";
import style from "./cloudTypes.module.scss";

const iconText = "CONTINUE READING";

class CloudTile extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text : <span className={"mt-4 mb-4 text-center d-flex"}>
             <i className={"fal fa-long-arrow-right fa-2x " +style.icon}></i>&nbsp;</span> 
             }
    }

   onMouseover (e) {
    this.setState({
        text : <span className={"mt-4 mb-4 text-center d-flex align-items-center " +style.icon}>
        {iconText} &nbsp;
        <i className={"fal fa-long-arrow-right fa-2x"}>&nbsp;</i>
        </span>
    });
  }

  onMouseout (e) {
    this.setState({text : <span className={"mt-4 mb-4 text-center d-flex"}>
                <i className={"fal fa-long-arrow-right fa-2x " + style.icon}>&nbsp;</i></span>})
  }

    render() {
        var obj = this.props.cloudTypeSolutions;
        var i = this.props.index;
        return (
            <a onMouseEnter={this.onMouseover.bind(this, i, this.props)}
            onMouseLeave={this.onMouseout.bind(this, i, this.props)} key={obj.id} href={obj.link} target="_blank" rel="noopener noreferrer">
                <div className={style[(i + 1) % 2 === 0 ? 'cloudTypes-tile-alt' : 'cloudTypes-tile']} >
                    <div className={style[(i + 1) % 2 === 0 ? 'tile-content-alt' : 'tile-content']}>
                        <div className={style.header}>
                            <h3><b>{obj.title}</b></h3>

                            <p>{obj.description}</p>
                        </div>
                        <div className={style.desktop} >
                           <b>{this.state.text}</b>
                        </div>
                        <div className={style.tablet}>
                            <b>
                                <span className={"mt-4 text-center d-flex align-items-center " +style.icon}>
                                    {iconText} &nbsp;
                                    <i className={"fal fa-long-arrow-right fa-2x"}></i>
                                </span>
                            </b>
                        </div>
                    </div>
                    <div className={style[(i + 1) % 2 === 0 ? 'tile-img-alt' : 'tile-img']}>
                        <img className={style['img']} src={obj.image} alt=""/>
                    </div>
                </div>
            </a>
        );
    }
}

export default CloudTile;