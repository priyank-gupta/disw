import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
//bootstrap Jquery required
import 'jquery/dist/jquery.min.js'
import 'popper.js/dist/popper.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-pro/css/all.min.css';

import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import React from 'react';

import { hydrate, render } from "react-dom";

const rootElement = document.getElementById("root");
if (rootElement.hasChildNodes()) {
  hydrate(<App />, rootElement);
} else {
  render(<App />, rootElement);
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
