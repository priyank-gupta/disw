import React from "react";
import {Helmet} from "react-helmet";

class SEOtags extends React.Component {
  render () {
    const defaultTitle = "Siemens Digital Industries Software";
    const title = this.props.seoTitle ? this.props.seoTitle + " | " + defaultTitle  : defaultTitle;

    const defaultUrl = "http://www-qa.sw.siemens.com.s3-website.us-east-2.amazonaws.com"
    const canonicalUrl = this.props.canonicalUrl ? defaultUrl + this.props.canonicalUrl : defaultUrl;

    const twitterTitle = this.props.twitterTitle ? this.props.twitterTitle : title;
    const ogTitle = this.props.ogTitle ? this.props.ogTitle : title;

    const twitterDescription = this.props.twitterDescription ? this.props.twitterDescription : this.props.seoDescription;
    const ogDescription = this.props.ogDescription ? this.props.ogDescription : this.props.seoDescription;

    return (
        <div className="application">
            <Helmet>
                <meta charSet="utf-8" />
                {/* General SEO tags*/}
                <title>{title}</title>
                <meta name="description"  content={this.props.seoDescription} />

                <link rel="canonical" href={canonicalUrl} />
                
                {/* Twitter tags*/}
                <meta name="twitter:title" content={twitterTitle} />
                <meta name="twitter:description" content={twitterDescription} />
                {this.props.twitterImage && <meta name="twitter:site" content={defaultUrl + this.props.twitterImage} />}
              
                {/* Open Graph tags*/}
                <meta property="og:title" content={ogTitle} />
                <meta property="og:description" content={ogDescription} />
                {this.props.ogImage && <meta property="og:image" content={defaultUrl + this.props.ogImage}  />}
                <meta property="og:site_name" content={defaultTitle} />

            </Helmet>
        </div>
    );
  }
}
export default SEOtags;