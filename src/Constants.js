
// Navigation Structure for the site. Passed into Header.js where it"s consumed & rendered.
export const navigation = [
  {
    link: "/journeys",
    hasLink: false,
    title: "Journeys",
    dropdownItems: [
      {
        title: "Industrial Additive",
        link: "/journeys/industrial-additive/",
        description: "Realizing the dream of a 3-D printed automobile. The success behind Local Motors with a little help from Siemens.",
      },
      {
        title: "Digital Twin",
        link: "/journeys/digital-twin/",
        description: "Accelerating the development of fusion energy. See how Siemens and Tokamak Energy are making it possible."
      },
      {
        title: "Generative Engineering",
        link: "/journeys/generative-engineering/",
        description: "17-year-old student designs, tests and builds a highly streamlined prosthetic foot for injured U.S. marine."
      },
      {
        title: "Industrial Additive with Cloud",
        link: "/journeys/industrial-additive-cloud/",
        description: "America Makes partners with Siemens to drive adoption of additive manufacturing (via the Industrial Internet of Things)."
      },
      {
        title : "Industry 4.0",
        link : "/journeys/industry/",
        description: "Embracing Industry 4.0 and digital manufacturing: How Siemens helped Electrolux improve time-to-market and reduce production costs."
      },
      {
        title : "Electrical",
        link : "/journeys/electrical/",
        description: "From napkin sketch to a fully functional three-wheel electric automobile. Electra Meccanica and Siemens do the unimaginable."
      },
      
    ]
  },

  {
    link: "/portfolio",
    title: "Portfolio",
    dropdownItems: [
      {
        title: "Cloud",
        link: "/portfolio/cloud/",
        description: "Realize unprecedented scalability and flexibility with Siemens Cloud Solutions – advanced cloud technology uniquely designed to help you take advantage of the cloud at your own pace."
      },
      {
        title: "Mendix",
        link: "https://www.mendix.com/",
        description: "Build what you can imagine – from enterprise-grade apps to simple tools – for increased user productivity."
      },
      {
        title: "Mentor",
        link: "https://www.mentor.com/",
        description: "Electronic design automation software allows you to develop more cost-effective electronic systems, components and products."
      },
      {
        title: "MindSphere",
        link: "https://siemens.mindsphere.io/",
        description: "An open IIoT operating system connecting products, plants, systems and machines. Harness the wealth of your data with advanced analytics."
      },
      {
        title: "Siemens PLM Software",
        link: "https://www.plm.automation.siemens.com/global/en/",
        description: "Successful manufacturers are using Siemens product lifecycle management software for innovation and growth."
      },
    ]
  },
  // {
  //   link : "/resources/",
  //   title : "Resources",
  //   dropdownItems : [
  //     {
  //       title : "Events",
  //       link : "https://www.plm.automation.siemens.com/global/en/your-success/events/",
  //       description: "Description",
  //       displayPanel: true
  //     },
  //     {
  //       title : "Community",
  //       link : "https://community.plm.automation.siemens.com/",
  //       description: "Description"
  //     },
  //     {
  //       title : "Blog",
  //       link : "https://community.plm.automation.siemens.com/t5/Siemens-PLM-Corporate-Blog/bg-p/SiemensPLM-Corporate-Blog",
  //       description: "Description"
  //     },
  //     {
  //       title : "Newsroom",
  //       link : "https://www.plm.automation.siemens.com/global/en/our-story/newsroom/",
  //       description: "Description"
  //     }
  //   ]
  // },
  // {
  //   link : "/about",
  //   title : "About",
  //   dropdownItems : [
  //     {
  //       title : "History",
  //       link : "https://www.siemens.com/history/en/history-news.htm",
  //       description: "Description",
  //       displayPanel: true
  //     },
  //     {
  //       title : "Leadership Team",
  //       link : "https://www.plm.automation.siemens.com/global/en/our-story/leadership/",
  //       description: "Description"
  //     },
  //     {
  //       title : "Openness",
  //       link : "https://www.plm.automation.siemens.com/global/en/our-story/openness/",
  //       description: "Description"
  //     },
  //     {
  //       title : "Quality",
  //       link : "https://www.plm.automation.siemens.com/global/en/our-story/quality.html",
  //       description: "Description"
  //     },
  //     {
  //       title : "Careers",
  //       link : "https://jobs.siemens-info.com/jobs?page=1",
  //       description: "Description"
  //     }
  //   ]
  // },
  {
    link: "/buy/",
    title: "Buy",
    hasLink: false,
    dropdownItems: [
      {
        title: "Siemens PLM Software",
        link: "https://www.dex.siemens.com/?selected=plm",
        description: "Purchase product lifecycle management software for innovation, growth and faster time-to-market."
      },
      {
        title: "MindSphere",
        link: "https://www.dex.siemens.com/?selected=mindsphere",
        description: "Purchase an open IoT operating system that harnesses data from your products, plants, systems and machines."
      },
      {
        title: "Mentor",
        link: "https://www.mentor.com/products/mechanical/where-to-buy",
        description: "Purchase electronic design automation software for more efficient design, test and manufacturing of electrical systems and integrated circuits."
      },
      {
        title: "Mendix",
        link: "https://www.mendix.com/pricing/",
        description: "Purchase a low-code application development platform based on the number of apps and users in your company."
      },
    ]
  },
];

export const relatedLinkIcon = {
  "Community": "user-friends",
  "Whitepaper": "file-invoice",
  "Blogpost": "comment-alt-dots",
  "Newsroom": "book-open",
  "Success Story": "thumbs-up",
  "Podcast": "podcast",
};

export const journeyCarouselTitle = "Discover more customer journeys…";
export const journeyCarouselItems = [
  {
    company: "Local Motors", icon: "fa-layer-plus", link: "/journeys/industrial-additive/", linkText: "Industrial Additive"
  },
  {
    company: "Tokamak Energy", icon: "fa-object-ungroup", link: "/journeys/digital-twin/", linkText: "Digital Twin"
  },
  {
    company: "Ashley Kimbel", icon: "fal fa-sync", link: "/journeys/generative-engineering/", linkText: "Generative Engineering"
  },
  {
    company: "America Makes", icon: "fa-cloud-upload", link: "/journeys/industrial-additive-cloud/", linkText: "Industrial Additive with Cloud"
  },
  {
    company: "Electrolux", icon: 'fa-industry-alt', link: '/journeys/industry', linkText: 'Industry 4.0'
  },
  {
    company: "Electra Meccanica", icon: 'fa-bolt', link: '/journeys/electrical', linkText: 'Electrical'
  }
];