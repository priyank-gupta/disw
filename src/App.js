import React from "react";
import Header from "./shared/Header/Header";
import Footer from "./shared/Footer/Footer";
import Home from "./pages/Home/Home";
import { BrowserRouter, Route } from "react-router-dom";
import styles from "./App.module.scss";
import LocalMotors from "./pages/Journeys/LocalMotors/LocalMotors";
import TokamakEnergy from "./pages/Journeys/TokamakEnergy/TokamakEnergy";
import HighSchoolStudent from "./pages/Journeys/HighSchoolStudent/HighSchoolStudent";
import AmericaMakes from "./pages/Journeys/AmericaMakes/AmericaMakes";
import ElectraMeccanica from "./pages/Journeys/ElectraMeccanica/ElectraMeccanica";
import Electrolux from "./pages/Journeys/Electrolux/Electrolux";
import Portfolio from "./pages/Portfolio/Portfolio";
import Cloud from "./pages/Portfolio/Cloud/Cloud";
import Xcelerator from "./pages/Portfolio/Xcelerator/Xcelerator";

function App() {

  return (
    <BrowserRouter forceRefresh={true}>
    <div className={styles.ie_fixMinHeight}>
      <div className={styles.wrapper}>
          <Header />
          <div className={styles.content}>
            <Route path="/" exact component={Home} />
            <Route path="/journeys/industrial-additive" component={LocalMotors} />
            <Route path="/journeys/digital-twin" component={TokamakEnergy} />
            <Route path="/journeys/generative-engineering" component={HighSchoolStudent} />
            <Route path="/journeys/industrial-additive-cloud" component={AmericaMakes} />
            <Route path="/journeys/electrical" component={ElectraMeccanica} />
            <Route path="/journeys/industry" component={Electrolux} />
            <Route path="/portfolio" exact component={Portfolio} />
            <Route path="/portfolio/cloud" exact component={Cloud} />
            <Route path="/portfolio/xcelerator" exact component={Xcelerator} />
          </div>

          {/* Dark overlay when navigation is expanded */}
          <div id="bg-overlay"></div>
          <Footer /> 
      </div>

    </div>
    </BrowserRouter>
  );
}

export default App;